<?php

function moneyConvert($money){
	if($money >= 10000){
		$money = number_format($money);
	}

	return $money;
}

function dateConvert($timestamp){
	$date = date('d.m.Y в H:i', $timestamp);

	if((time() - $timestamp) < 3600){
		$difference = time() - $timestamp;
		$min = round($difference / 60);

		if($min == 0) $date = 'только что';
		else $date = $min.' минут назад';
	} elseif(date('d.m.Y', time()) == date('d.m.Y', $timestamp)){
		$date = 'сегодня в '.date('H:i', $timestamp);
	} elseif(date('m.Y', time()) == date('m.Y', $timestamp) && (intval(date('d', time())) - 1) == intval(date('d', $timestamp))){
		$date = 'вчера в '.date('H:i', $timestamp);
	} else {
		$months = [
			1 => 'января',
			2 => 'февраля',
			3 => 'марта',
			4 => 'апреля',
			5 => 'мая',
			6 => 'июня',
			7 => 'июля',
			8 => 'августа',
			9 => 'сентября',
			10 => 'октября',
			11 => 'ноября',
			12 => 'декабря',
		];

		if(date('Y', time()) == date('Y', $timestamp)) $date = date('j', $timestamp).' '.$months[date('n', $timestamp)].' в '.date('H:i', $timestamp);
		else $date = date('j', $timestamp).' '.$months[date('n', $timestamp)].' '.date('Y', $timestamp).' в '.date('H:i', $timestamp);
	}

	return $date;
}

function timeConvert($seconds){
	if($seconds < 60) return 1;
	if($seconds > 60){
		$minuts = round($seconds / 60);

		// if($minuts > 60){
		// 	$hours = round($minuts / 60);
		// 	$minuts = $minuts % 60;

		// 	return $hours.' часов '.$minuts.' минут';
		// }

		return $minuts;
	}
}

function getEngineVersion(){
	return '0.2.9';
}
