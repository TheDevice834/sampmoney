<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Cost;

class AccountController extends Controller
{
    public function balance(){
        $user = Auth::user();
        $costs = Cost::where('user_id', $user->id)->orderBy('id', 'DESC')->paginate(25);

        return view('account.balance', [
            'title' => 'Баланс',
            'user' => $user,
            'costs' => $costs
        ]);
    }

    public function settings(Request $request){
		$user = Auth::user();

    	if($request->isMethod('post')){
    		$rules = [
    			'vk_link' => 'required',
                'show_name' => 'required'
    		];

    		$validator = Validator::make($request->all(), $rules);

    		if($validator->fails()){
    			return redirect()->route('settings')->withErrors($validator);
    		}

    		$vk_link = intval($request->input('vk_link'));
    		if($vk_link != 0 && $vk_link != 1){
    			return redirect()->route('settings')->withErrors(['Wrong option.']);
    		}

            $show_name = intval($request->input('show_name'));
            if($show_name != 0 && $show_name != 1){
                return redirect()->route('settings')->withErrors(['Wrong option.']);
            }

    		$user->show_vk_link = $vk_link;
            $user->show_name = $show_name;
    		$user->save();

    		return redirect()->route('settings')->with('success', 'Настройки сохранены.');
    	} else return view('account.settings', [
    		'title' => 'Настройки',
    		'user' => $user
    	]);
    }
}
