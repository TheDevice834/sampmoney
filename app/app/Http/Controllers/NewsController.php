<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\User;
use Auth;

class NewsController extends Controller
{
    public function index(){
        if(Auth::check()) $news = News::where('access_level', '<', 2)->orderBy('id', 'DESC')->paginate(10);
        else $news = News::where('access_level', '<>', 1)->orderBy('id', 'DESC')->paginate(10);

        return view('news.index', [
            'title' => 'Новости',
            'news' => $news
        ]);
    }

    public function view($post_id){
        $post = News::findOrFail($post_id);

        if(Auth::check() && $post->access_level == 2) return abort(404);
        if(!Auth::check() && $post->access_level == 1) return abort(404);

        $author = User::find($post->author_id);

        return view('news.view', [
            'title' => $post->title,
            'news' => $post,
            'author' => $author
        ]);
    }
}
