<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Notice;
use App\Subscriber;
use App\Wall;
use App\Complaint;
use Validator;
use Auth;

class UsersController extends Controller
{
    public function view($user_id){
        $user = User::findOrFail($user_id);
        $auth_user = Auth::user();

        $subscribers = Subscriber::where('subscription_id', $user->id)->orderBy('id', 'DESC')->get();
        $subscriptions = Subscriber::where('subscriber_id', $user->id)->orderBy('id', 'DESC')->get();

        $friends = [];

        foreach($subscribers as $subscriber){
            foreach($subscriptions as $subscription){
                if($subscriber->subscriber_id == $subscription->subscription_id){
                    $friends[] = User::find($subscriber->subscriber_id);
                    break;
                }
            }
        }

        if($subscriber = Subscriber::where('subscriber_id', $auth_user->id)->where('subscription_id', $user->id)->first()){
            if(Subscriber::where('subscriber_id', $user->id)->where('subscription_id', $auth_user->id)->first()){
                $friend_status = 2;
            } else $friend_status = 1;
        } else {
            $friend_status = 0;
        }

        $wall = $user->getWall();

        return view('users.view', [
            'title' => $user->getFullName(),
            'user' => $user,
            'auth_user' => $auth_user,
            'friend_status' => $friend_status,
            'friends' => $friends,
            'wall' => $wall
        ]);
    }

    public function addFriend($user_id){
        $user = User::findOrFail($user_id);
        $auth_user = Auth::user();

        if($user->id == $auth_user->id) return abort(404);

        $friend_status = 0;

        if(!Subscriber::where('subscriber_id', $auth_user->id)->where('subscription_id', $user->id)->first()){
            $subscriber = Subscriber::create([
                'subscriber_id' => $auth_user->id,
                'subscription_id' => $user->id
            ]);

            Notice::create([
                'user_id' => $user->id,
                'type_id' => 1,
                'short_text' => sprintf('%s подписался на Вас.', $auth_user->getFullName()),
                'full_text' => sprintf('<a href="%s">%s</a> подписался на Вас.', route('users.view', ['user_id' => $auth_user->id]), $auth_user->getFullName())
            ]);

            if(Subscriber::where('subscriber_id', $user->id)->where('subscription_id', $auth_user->id)->first()){
                $friend_status = 2;
            } else $friend_status = 1;
        }

        return json_encode(['success' => true, 'state' => $friend_status]);
    }

    public function removeFriend($user_id){
        $user = User::findOrFail($user_id);
        $auth_user = Auth::user();

        if($subscriber = Subscriber::where('subscriber_id', $auth_user->id)->where('subscription_id', $user->id)->first()){
            $subscriber->delete();
        }

        return json_encode(['success' => true, 'state' => 0]);
    }

    public function makeComplaint(Request $request){
        $user = Auth::user();

        $rules = [
            'culprit_id' => 'required',
            'cause' => 'required',
            'object' => 'required',
            'object_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) return json_encode(['success' => false, 'message' => 'Hacked attempt.']);

        $culprit_id = intval($request->input('culprit_id'));
        $cause = intval($request->input('cause'));
        $object = intval($request->input('object'));
        $object_id = intval($request->input('object_id'));

        if($user->id == $culprit_id) return json_encode(['success' => false, 'message' => 'Вы не можете отправить жалобу на себя.']);

        if(!isset(Complaint::getCauses()[$cause]) || !isset(Complaint::getObjects()[$object])) return json_encode(['success' => false, 'message' => 'Hacked attempt.']);

        if(Complaint::create([
            'author_id' => $user->id,
            'culprit_id' => $culprit_id,
            'cause' => $cause,
            'object' => $object,
            'object_id' => $object_id,
        ])){
            return json_encode(['success' => true, 'message' => 'Ваша жалоба отправлена администрации и будет рассмотрена в ближайшее время.']);
        } else return json_encode(['success' => false, 'message' => 'Что-то пошло не так. Попробуйте позже.']);

    }

    public function find($user_id){
        if($user = User::find($user_id)) return json_encode([
            'found' => true,
            'user' => [
                'id' => $user->id,
                'full_name' => $user->getFullName(),
                'avatar' => $user->avatar,
                'url' => route('users.view', ['user_id' => $user->id])
            ]
        ]);
        else return json_encode(['found' => false]);
    }
}
