<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notice;
use Auth;

class NoticeController extends Controller
{
    public function list(){
        $user = Auth::user();

        $unread = Notice::where('user_id', $user->id)->where('is_read', 0)->count();
        $notices = Notice::where('user_id', $user->id)->orderBy('id', 'DESC')->paginate(20);

        return view('notices.list', [
            'title' => 'Уведомления',
            'user' => $user,
            'unread' => $unread,
            'notices' => $notices
        ]);
    }

    public function view($notice_id){
        $notice = Notice::findOrFail($notice_id);
        $user = Auth::user();
        if($notice->user_id != $user->id) return abort(404);

        return view('notices.view', [
            'title' => sprintf('Уведомление #%d: %s', $notice->id, $notice->getTypeInfo()['title']),
            'user' => $user,
            'notice' => $notice
        ]);
    }
}
