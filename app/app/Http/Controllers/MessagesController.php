<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Dialog;
use App\Message;
use App\Advert;
use App\User;
use App\Deal;
use Validator;

class MessagesController extends Controller
{
    public function messages(){
        $user = Auth::user();

        $dialogs = Dialog::getDialogList($user->id);

        return view('messages.list', [
            'title' => 'Сообщения',
            'dialogs' => $dialogs,
            'user' => $user
        ]);
    }

    public function dialog($dialog_id){
        $user = Auth::user();
        $dialog = Dialog::getDialog($dialog_id, $user->id);

        if(isset($dialog['security']) && $dialog['security'] == false) return abort(404);

        foreach($dialog['messages'] as $message){
            if($message->sender_id != $user->id || $dialog['members']->id == $user->id){
                $message->is_read = 1;
                $message->save();
            }
        }

        $deal = [];

        foreach(Deal::where('user_id', $user->id)->where('status', '<>', 4)->get() as $item){
            if($advert = Advert::find($item->advert_id)){
                if($advert->user_id == $dialog['members']->id){
                    $deal['deal'] = $item;
                    $deal['advert'] = $advert;
                    $deal['seller'] = User::find($advert->user_id);
                    break;
                }
            }
        }

        if(empty($deal)){
            foreach(Deal::where('user_id', $dialog['members']->id)->where('status', '<>', 4)->get() as $item){
                if($advert = Advert::find($item->advert_id)){
                    if($advert->user_id == $user->id){
                        $deal['deal'] = $item;
                        $deal['advert'] = $advert;
                        $deal['seller'] = User::find($advert->user_id);
                        break;
                    }
                }
            }
        }

        if(empty($deal) && $dialog['is_garant'] == true){
            foreach(Deal::where('user_id', $dialog['members']->id)->where('status', '<>', 4)->get() as $item){
                if($advert = Advert::find($item->advert_id)){
                    $deal['deal'] = $item;
                    $deal['advert'] = $advert;
                    $deal['seller'] = User::find($advert->user_id);
                    break;
                }
            }
            if(empty($deal)){
                foreach(DB::table('dialog_members')->where('dialog_id', $dialog_id)->get() as $member){
                    if($deal = Deal::where('user_id', $member->user_id)->where('garant', $user->id)->where('status', '<>', 4)->first()){
                        if($advert = Advert::find($deal->advert_id)){
                            if($advert->user_id == $dialog['members']->id){
                                $deal['deal'] = $deal;
                                $deal['advert'] = $advert;
                                $deal['seller'] = User::find($advert->user_id);
                                break;
                            }
                        }
                    }
                }
            }
        }

        if($dialog['is_garant'] == true){
            $title = 'Сделка #'.$deal['deal']->id;
        } else $title = 'Диалог с '.$dialog['members']->getFullName();

        return view('messages.dialog', [
            'title' => $title,
            'dialog' => $dialog,
            'dialog_id' => $dialog_id,
            'user' => $user,
            'deal' => $deal
        ]);
    }

    public function send(Request $request){
        if($request->isMethod('post')){
            $user = Auth::user();

            $rules = [
                'recipient' => 'required',
                'message' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                return redirect()->route('messages.send')->withErrors($validator)->withInput();
            }

            $recipient_id = intval($request->input('recipient'));
            $message = $request->input('message');

            if($request->filled('dialog')){
                $dialog_id = intval($request->input('dialog'));

                $message = Message::send($user->id, $recipient_id, $message, NULL, 0, $dialog_id);

                return redirect()->route('messages.dialog', ['dialog_id' => $message->dialog_id]);
            }

            $message = Message::send($user->id, $recipient_id, $message);

            return redirect()->route('messages.dialog', ['dialog_id' => $message->dialog_id]);
        } else {
            if($request->input('user') !== false) $user_id = $request->input('user');

            return view('messages.send', [
                'title' => 'Отправка сообщения',
                'user_id' => $user_id
            ]);
        }
    }
}