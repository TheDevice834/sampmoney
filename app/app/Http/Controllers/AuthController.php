<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use VK\Client\VKApiClient;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;
use VK\OAuth\Scopes\VKOAuthUserScope;
use Auth;
use App\User;
use App\Notice;
use App\Activity;

class AuthController extends Controller
{
    public function auth(Request $request){
        $oAuth = new VKOAuth();
        $client_id = env('VK_APP_CLIENT_ID');
        $redirect_uri = route('auth');
        $display = VKOAuthDisplay::PAGE;
        $scope = [
            VKOAuthUserScope::EMAIL,
        ];

        $authUrl = $oAuth->getAuthorizeUrl(VKOAuthResponseType::CODE, $client_id, $redirect_uri, $display, $scope);

        if($request->input('code') === NULL) return redirect($authUrl);

        $client_secret = env('VK_APP_CLIENT_SECRET');
        $code = $request->input('code');

        $access_token = $oAuth->getAccessToken($client_id, $client_secret, $redirect_uri, $code);

        if(!isset($access_token['email'])) die('Ошибка. E-Mail не найден.');

        $vk = new VKApiClient();
        $user_info = $vk->users()->get($access_token['access_token'], array(
            'user_id' => $access_token['user_id'],
            'fields' => ['photo_200']
        ));
        $user_info = $user_info[0];

        $user = User::where('vk_id', $user_info['id'])->first();

        if(!$user){
            if($new_user = User::create([
                'vk_id' => $user_info['id'],
                'first_name' => $user_info['first_name'],
                'last_name' => $user_info['last_name'],
                'email' => $access_token['email'],
                'avatar' => $user_info['photo_200'],
                'access_token' => $access_token['access_token'],
                'expires_in' => time() + $access_token['expires_in'],
                'last_activity' => time()
            ])){
                Notice::create([
                    'user_id' => $new_user->id,
                    'type_id' => 0,
                    'short_text' => 'Регистрация успешно завершена.',
                    'full_text' => 'Регистрация успешно завершена. Теперь Вы можете пользоваться всеми услугами нашего сайта.'
                ]);

                Activity::create([
                    'user_id' => $new_user->id,
                    'text' => 'Зарегистрировался! Приветствуем.'
                ]);

                Auth::login($new_user, true);

                return redirect()->route('home');
            } else die('Упс. Что-то пошло не так. Попробуйте ещё раз.');
        } else {
            Auth::login($user, true);

            return redirect()->route('home');
        }
    }

    public function logout(){
        Auth::logout();

        return redirect()->route('home');
    }
}
