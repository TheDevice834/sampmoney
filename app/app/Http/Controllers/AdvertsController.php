<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use GuzzleHttp\Client;
use Validator;
use DB;
use Auth;
use App\User;
use App\Advert;
use App\Activity;
use App\Message;
use App\Notice;
use App\Deal;

class AdvertsController extends Controller
{
    public function index(Request $request){
        $filter_on = false;
        $filter = [];

        if($request->filled('type')){
            $type = intval($request->input('type'));
            $types = Advert::getTypes();

            if(isset($types[$type])){
                $adverts = Advert::where('type', $type)->where('is_hide', 0)->orderBy('id', 'DESC')->paginate(20);
                $filter_on = true;
                $filter = [
                    'name' => 'по типу объявления: <b>'.$types[$type].'</b>',
                    'found' => Advert::where('type', $type)->where('is_hide', 0)->count()
                ];
            }
        }
        elseif($request->filled('server')){
            $server = $request->input('server');

            $filter_on = true;
            $filter['name'] = 'по серверу: <b>'.$server.'</b>';

            if($server_obj = DB::table('servers')->where('name', $server)->first()){
                $adverts = Advert::where('server', $server_obj->id)->where('is_hide', 0)->orderBy('id', 'DESC')->paginate(20);
                $filter['found'] = Advert::where('server', $server_obj->id)->where('is_hide', 0)->count();
            } else {
                $adverts = Advert::where('server_name', $server)->where('is_hide', 0)->orderBy('id', 'DESC')->paginate(20);
                $filter['found'] = Advert::where('server_name', $server)->where('is_hide', 0)->count();
            }
        }

        if(!isset($adverts)) $adverts = Advert::orderBy('id', 'DESC')->where('is_hide', 0)->paginate(20);

        $adverts_count = Advert::where('is_hide', 0)->count();

        return view('adverts.index', [
            'title' => 'Объявления',
            'adverts' => $adverts,
            'adverts_count' => $adverts_count,
            'filter_on' => $filter_on,
            'filter' => $filter
        ]);
    }

    public function view($advert_id){
        $advert = Advert::findOrFail($advert_id);
        $auth = Auth::user();

        if($advert->is_hide == 1 && $auth->id != $advert->user_id && !$deal = Deal::where('user_id', $auth->id)->where('advert_id', $advert->id)->where('status', '>=', 2)->first()){
            if($auth->garant != 1 && $deal->garant != 0) return abort(404);
        }

        if(isset($advert->server)){
            $server_obj = DB::table('servers')->find($advert->server);

            $server = $server_obj->name;
        } else $server = $advert->server_name;

        $author = User::find($advert->user_id);

        if(isset($advert->server_name)) $server = $advert->server_name;
        else {
            $server_obj = DB::table('servers')->find($advert->server);
            $server = $server_obj->name;
        }

        $title = Advert::getTypes()[$advert->type].' $'.moneyConvert($advert->amount).' на '.$server;

        return view('adverts.view', [
            'title' => $title,
            'advert' => $advert,
            'server' => $server,
            'author' => $author,
            'auth' => $auth
        ]);
    }

    public function startDeal($advert_id){
        $user = Auth::user();
        $advert = Advert::findOrFail($advert_id);

        if($user->id == $advert->user_id) return abort(403);

        if(Deal::where('advert_id', $advert->id)->where('status', '<>', 4)->where('user_id', $user->id)->first()) return redirect()->route('adverts.view', ['advert_id' => $advert->id])->withErrors(['Вы уже проводите сделку по данному объявлению.']);

        if($advert->is_hide == 1) return abort(403);

        $deal = Deal::create([
            'user_id' => $user->id,
            'advert_id' => $advert->id
        ]);

        Notice::create([
            'user_id' => $user->id,
            'type_id' => 5,
            'short_text' => 'Вы успешно начали сделку #'.$deal->id.'.',
            'full_text' => 'Вы успешно начали сделку <a href="'.route('deal.redirect', ['deal_id' => $deal->id]).'">#'.$deal->id.'</a>.'
        ]);

        if(isset($advert->server)){
            $server_obj = DB::table('servers')->find($advert->server);

            $server = $server_obj->name;
        } else $server = $advert->server_name;

        $message_text = 'Вы начали сделку.
        Объявление: <a href="'.route('adverts.view', ['advert_id' => $advert->id]).'" target="_blank">$'.moneyConvert($advert->amount).' на '.$server.'</a>.
        К оплате: <b>'.moneyConvert($advert->price).' руб.</b>

        <a href="'.route('deal.pay', ['deal_id' => $deal->id]).'" class="btn btn-success btn-sm"><b>Оплатить</b></a>';

        $message = Message::send($user->id, $advert->user_id, $message_text, 1, $user->id);

        return redirect()->route('messages.dialog', ['dialog_id' => $message->dialog_id]);
    }

    public function add(Request $request){
    	if($request->isMethod('post')){
    		$rules = [
    			'type' => [
    				'required',
    				Rule::notIn(['0']),
    			],
    			'server' => [
    				'required',
    				Rule::notIn(['0']),
    			],
    			'amount' => 'required|numeric|min:1',
    			'transfer_method' => [
    				'required',
    				Rule::notIn(['0']),
    			],
    			'price' => 'required|numeric|min:1',
                'g-recaptcha-response' => 'required'
    		];
    		$messages = [
    			'not_in' => 'Вы заполнили не все поля.',
    			'required' => 'Вы заполнили не все поля.',
    			'project_name.required_if' => 'Вы не указали название проекта.',
                'amount.min' => 'Минимальное количество виртов – 1 вирт',
                'price.min' => 'Минимальная стоимость – 1 руб.',
                'g-recaptcha-response.required' => 'Вы не прошли проверку на робота.'
    		];

    		$validator = Validator::make($request->all(), $rules, $messages);

    		if($validator->fails()){
    			return redirect()->route('adverts.add')->withErrors($validator);
    		}

            $client = new Client();

            $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => [
                    'secret' => env('RECAPTCHA_SECRET'),
                    'response' => $request->input('g-recaptcha-response')
                ]
            ]);

            $body = json_decode($response->getBody(),true);

            if($body['success'] == false) return redirect()->route('adverts.add')->withErrors(['Вы не прошли проверку на робота.']);

    		$type = intval($request->input('type'));
    		$server = $request->input('server');
    		if($server == 'another') $project_name = $request->input('project_name');
    		$amount = intval($request->input('amount'));
    		$transfer_method = intval($request->input('transfer_method'));
    		$price = intval($request->input('price'));

            $errors = [];

            $types = Advert::getTypes();
            if(!isset($types[$type])) $errors[] = 'Неверный тип.';

            if(!isset($project_name)){
                $server = intval($server);

                if(!$selected_server = DB::table('servers')->find($server)){
                    $errors[] = 'Неверный проект.';
                }

                $server_field = 'server';
                $server_value = $server;
            } else {
                if($project_name == '') $errors[] = 'Вы не ввели название сервера.';
                if(strlen($project_name) > 190) $errors[] = 'Максимальная длина названия сервера – 190 символов.';

                $server_field = 'server_name';
                $server_value = $project_name;
            }

            $transfer_methods = Advert::getTransferMethods();
            if(!isset($transfer_methods[$transfer_method])) $errors[] = 'Неверный способ передачи/получения виртов.';

            if(!empty($errors)){
                return redirect()->route('adverts.add')->withErrors($errors);
            }

            if(isset($selected_server)) $activity_server = $selected_server->name;
            else $activity_server = $project_name;

            $user = Auth::user();

            if($advert = Advert::create([
                'user_id' => $user->id,
                'type' => $type,
                $server_field => $server_value,
                'amount' => $amount,
                'transfer_method' => $transfer_method,
                'price' => $price,
            ])){
                Activity::create([
                    'user_id' => $user->id,
                    'text' => sprintf('Опубликовал объявление #%d: %s $%s за %s руб. на %s', $advert->id, Advert::getTypes()[$type], moneyConvert($advert->amount), moneyConvert($advert->price), $activity_server)
                ]);

                return redirect()->route('adverts.view', ['advert_id' => $advert->id]);
            }
            else return redirect()->route('adverts.add')->withErrors(['Что-то пошло не так. Попробуйте позже.']);
    	} else {
    		$servers = DB::table('servers')->get();
    		$transfer_methods = Advert::getTransferMethods();
            $types = Advert::getTypes();

    		return view('adverts.add', [
    			'title' => 'Подача объявления',
    			'servers' => $servers,
    			'transfer_methods' => $transfer_methods,
                'types' => $types
    		]);
    	}
    }

    public function delete($advert_id){
        $auth = Auth::user();
        $advert = Advert::findOrFail($advert_id);

        if($auth->id != $advert->user_id) return abort(404);

        if($deal = Deal::where('advert_id', $advert->id)->first()){
            return redirect()->route('adverts.view', ['advert_id' => $advert->id])->withErrors(['Вы не можете удалить это объявление, так как есть активная сделка по данному объявлению.']);
        }

        $advert->delete();

        return redirect()->route('home')->withErrors(['Объявление удалено.']);
    }
}
