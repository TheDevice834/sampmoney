<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Advert;
use App\Deal;
use App\Notice;
use App\Message;
use App\User;
use App\Cost;

class DealController extends Controller
{
    public function cancel($deal_id){
        $deal = Deal::findOrFail($deal_id);
        $advert = Advert::findOrFail($deal->advert_id);
        $user = Auth::user();

        if($deal->user_id != $user->id) return abort(404);

    	if($deal->status == 1){
            Notice::create([
                'user_id' => $deal->user_id,
                'type_id' => 5,
                'short_text' => 'Сделка #'.$deal->id.' отменена.',
                'full_text' => 'Сделка <b>#'.$deal->id.'</b> отменена.'
            ]);

            $deal->delete();

            $advert->is_hide = 0;
            $advert->save();

            return redirect()->route('adverts.view', ['advert_id' => $advert->id])->withErrors(['Сделка отменена.']);
        } else return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);
    }

    public function transfer($deal_id){
        $deal = Deal::findOrFail($deal_id);
        $advert = Advert::findOrFail($deal->advert_id);
        $user = Auth::user();

        if($deal->user_id == $user->id) $user_role = 1;
        elseif($advert->user_id == $user->id) $user_role = 2;
        else return abort(404);

        if($user_role == 1 && $deal->status != 3) return abort(404);
        if($user_role == 2 && $deal->status != 2) return abort(404);

        if($user_role == 1 && $deal->status == 3){
            // Статус сделки: Завершена
            $deal->status = 4;
            $deal->garant = null;
            $deal->save();

            // Открытие объявления для всех
            $advert->is_hide = 0;
            $advert->save();

            // Начисление средств на счет продавца
            $seller = User::find($advert->user_id);
            $seller->balance = $seller->balance + $advert->price;
            $seller->save();

            Cost::create([
                'user_id' => $seller->id,
                'type' => 1,
                'text' => 'Завершенная сделка #'.$deal->id,
                'amount' => $advert->price
            ]);

            // Продавцу
            $message_text = 'Покупатель подтвердил получение товара.
            Сделка завершена.
            <b><span style="color: #0ad251;">'.$advert->price.' руб.</span></b> переведены на Ваш счет.';
            $seller_message = Message::send($deal->user_id, $advert->user_id, $message_text, 1, $advert->user_id);

            Notice::create([
                'user_id' => $advert->user_id,
                'type_id' => 5,
                'short_text' => 'Покупатель подтвердил получение товара.',
                'full_text' => 'Покупатель подтвердил получение товара по сделке #'.$deal->id.'. Средства переведены на Вас счет.'
            ]);

            // Покупателю
            $message_text = 'Вы подтвердили получение товара
            Сделка завершена.
            Средства переведены на счет продавца.';
            $message = Message::send($deal->user_id, $advert->user_id, $message_text, 1, $deal->user_id);

            // Удаление гаранта из диалога
            DB::table('dialog_members')->where('dialog_id', $message->dialog_id)->where('role', 1)->delete();

            Notice::create([
                'user_id' => $deal->user_id,
                'type_id' => 5,
                'short_text' => 'Вы подтвердили передачу товара.',
                'full_text' => 'Вы подтвердили передачу товара по сделке #'.$deal->id.'. Средства переведены на счет продавца.'
            ]);

            return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);
        } elseif($user_role == 2 && $deal->status == 2) {
            $deal->status = 3;
            $deal->save();

            $message_text = 'Вы подтвердили передачу товара.
            Ожидайте подтверждения об успешном получении товара от покупателя.';
            $seller_message = Message::send($deal->user_id, $user->id, $message_text, 1, $user->id);

            $message_text = 'Продавец подтвердил передачу товара.
            Проверьте наличие товара и подтвердите успешное получение, нажав соответствующую кнопку.';
            $message = Message::send($deal->user_id, $user->id, $message_text, 1, $deal->user_id);

            Notice::create([
                'user_id' => $deal->user_id,
                'type_id' => 5,
                'short_text' => 'Продавец подтвердил передачу товара.',
                'full_text' => 'Продавец подтвердил передачу товара по сделке <a href="'.route('deal.redirect', ['deal_id' => $deal->id]).'">#'.$deal->id.'</a>.'
            ]);
        }

        return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);
    }

    public function pay($deal_id){
        $deal = Deal::findOrFail($deal_id);
        $user = Auth::user();

        if($deal->user_id != $user->id) return abort(404);

        if($deal->status != 1) return abort(404);

        $advert = Advert::findOrFail($deal->advert_id);

        if($user->balance >= $advert->price){
            $user->balance -= $advert->price;
            $user->save();
        } else {
            $message_text = 'Недостаточно средств на балансе для оплаты.';
            $message = Message::send($user->id, $advert->user_id, $message_text, 1, $user->id);

            return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);
        }

        Cost::create([
            'user_id' => $user->id,
            'type' => 2,
            'text' => 'Оплата товара по сделке #'.$deal->id,
            'amount' => $advert->price
        ]);

        $deal->status = 2;
        $deal->save();

        $advert->is_hide = 1;
        $advert->save();

        $message_text = 'Вы успешно оплатили товар.
        Уведомление о сделке отправлено продавцу.';

        $message = Message::send($user->id, $advert->user_id, $message_text, 1, $user->id);

        $message_text = 'Покупатель оплатил Ваш товар.
        Свяжитесь с ним, чтобы договорится о передаче товара.
        После успешой передачи нажмите на соответствующую кнопку, чтобы покупатель был уведомлен.';

        $seller_message = Message::send($user->id, $advert->user_id, $message_text, 1, $advert->user_id);

        Notice::create([
            'user_id' => $advert->user_id,
            'type_id' => 5,
            'short_text' => 'Покупатель оплатил Ваш товар.',
            'full_text' => 'Покупатель оплатил Ваш товар. <a href="'.route('deal.redirect', ['deal_id' => $deal->id]).'">Свяжитесь</a> с ним.'
        ]);

        return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);
    }

    public function redirectToDealDialog($deal_id){
        $deal = Deal::findOrFail($deal_id);
        $advert = Advert::findOrFail($deal->advert_id);
        $user = Auth::user();

        if($deal->user_id == $user->id){
            $first_user = $user->id;
            $second_user = $advert->user_id;
        } elseif($advert->user_id == $user->id){
            $first_user = $deal->user_id;
            $second_user = $user->id;
        } elseif(isset($deal->garant) && $deal->garant == $user->id){
            $first_user = $deal->user_id;
            $second_user = $advert->user_id;
        } else return abort(404);

        if($dialog = Message::findDialog($first_user, $second_user)){
            return redirect()->route('messages.dialog', ['dialog_id' => $dialog->id]);
        } else return back();
    }
}
