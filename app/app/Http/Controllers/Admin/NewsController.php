<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\News;
use App\User;

class NewsController extends Controller
{
    public function index(){
        $news = News::orderBy('id', 'DESC')->paginate(20);

        foreach($news as $post){
            $post['author'] = User::find($post->author_id);
        }

        return view('admin.news.index', [
            'title' => 'Новости',
            'news' => $news
        ]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            $rules = [
                'title' => 'required|max:190',
                'access_level' => 'required',
                'text' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()) return redirect()->route('admin.news.add')->withErrors($validator)->withInput();

            $title = $request->input('title');
            $access_level = intval($request->input('access_level'));
            $text = $request->input('text');

            if($access_level != 0 && $access_level != 1 && $access_level != 2){
                return redirect()->route('admin.news.add')->withErrors(['Неверная видимость.'])->withInput();
            }

            $user = Auth::user();

            $news = News::create([
                'access_level' => $access_level,
                'author_id' => $user->id,
                'title' => $title,
                'text' => $text
            ]);

            return redirect()->route('admin.news')->with('success', 'Новость #'.$news->id.' опубликована.');
        }
        else {
            return view('admin.news.add', [
                'title' => 'Добавить новость'
            ]);
        }
    }

    public function edit(Request $request, $post_id){
        $post = News::findOrFail($post_id);

        if($request->isMethod('post')){
            $rules = [
                'title' => 'required|max:190',
                'access_level' => 'required',
                'text' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()) return redirect()->route('admin.news.edit', ['post_id' => $post->id])->withErrors($validator)->withInput();

            $title = $request->input('title');
            $access_level = intval($request->input('access_level'));
            $text = $request->input('text');

            $post->title = $title;
            $post->access_level = $access_level;
            $post->text = $text;
            $post->save();

            return redirect()->route('admin.news')->with('success', 'Новость сохранена.');
        } else {
            return view('admin.news.add', [
                'title' => 'Редактирование новости #'.$post->id,
                'post' => $post
            ]);
        }
    }

    public function delete($post_id){
        $post = News::findOrFail($post_id);
        $post->delete();

        return redirect()->route('admin.news')->withErrors(['Новость удалена.']);
    }
}
