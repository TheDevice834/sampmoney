<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\User;
use App\Advert;
use App\Notice;

class AdvertsController extends Controller
{
	public function index(){
		$adverts = Advert::orderBy('id', 'DESC')->paginate(20);

        foreach($adverts as $advert){
            $advert['author'] = User::find($advert->user_id);
        }

        return view('admin.adverts.index', [
            'title' => 'Объявления',
            'adverts' => $adverts
        ]);
	}

	public function delete($advert_id){
        $advert = Advert::findOrFail($advert_id);
        if($advert->is_hide == 1) return abort(403);
        
        $advert->delete();
        Notice::create([
            'user_id' => $advert->user_id,
            'type_id' => 6,
            'short_text' => 'Администратор удалил Ваше объявление #'.$advert->id.'.',
            'full_text' => 'Администратор удалил Ваше объявление <b>#'.$advert->id.'</b>.'
        ]);

        return redirect()->route('admin.adverts')->withErrors(['Объявление удалено.']);
    }
}