<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use DB;
use App\User;
use App\Notice;
use App\Message;
use App\Deal;
use App\Advert;
use App\Cost;

class CancelRequestsController extends Controller
{
	public function index(){
		$requests = DB::table('deal_cancelreq')->orderBy('id', 'DESC')->paginate(20);

        return view('admin.cancelreq.index', [
            'title' => 'Заявки на отмену сделки',
            'requests' => $requests
        ]);
	}

	public function reject($req_id){
		if(!$request = DB::table('deal_cancelreq')->where('id', $req_id)->first()) return abort(404);
		if($request->status != 0) return abort(404);

		$deal = Deal::findOrFail($request->deal_id);
		$advert = Advert::findOrFail($deal->advert_id);

		if($deal->status == 4){
			DB::table('deal_cancelreq')->where('id', $req_id)->delete();

			return redirect()->route('admin.cancelreq')->withErrors(['Сделка уже завершена. Заявка удалена.']);
		}

		DB::table('deal_cancelreq')->where('id', $req_id)->update([
			'status' => 2
		]);

		Notice::create([
            'user_id' => $request->garant_id,
            'type_id' => 5,
            'short_text' => 'Ваша заявка на отмену сделки #'.$request->deal_id.' отклонена администрацией.',
            'full_text' => 'Ваша заявка на отмену сделки <a href="'.route('deal.redirect', ['deal_id' => $request->deal_id]).'"><b>#'.$request->deal_id.'</b></a> отклонена администрацией.'
        ]);

		$message_text = 'Администрация отклонила заявку на отмену сделки.';
        $message = Message::send($deal->user_id, $advert->user_id, $message_text, 1, 0);

		return redirect()->route('admin.cancelreq')->withErrors(['Заявка отклонена']);
	}

	public function approve($req_id){
		if(!$request = DB::table('deal_cancelreq')->where('id', $req_id)->first()) return abort(404);
		if($request->status != 0) return abort(404);

		$deal = Deal::findOrFail($request->deal_id);
		$advert = Advert::findOrFail($deal->advert_id);

		if($deal->status == 4){
			DB::table('deal_cancelreq')->where('id', $req_id)->delete();

			return redirect()->route('admin.cancelreq')->withErrors(['Сделка уже завершена. Заявка удалена.']);
		}

		DB::table('deal_cancelreq')->where('id', $req_id)->update([
			'status' => 1
		]);

		$deal->status = 4;
        $deal->garant = null;
        $deal->save();

        // Открытие объявления для всех
        $advert->is_hide = 0;
        $advert->save();

        if($request->in_favor == 1){
        	$seller = User::find($advert->user_id);
	        $seller->balance = $seller->balance + $advert->price;
	        $seller->save();

	        Cost::create([
	            'user_id' => $seller->id,
	            'type' => 1,
	            'text' => 'Отмененная гарантом сделка #'.$deal->id,
	            'amount' => $advert->price
	        ]);

	        Notice::create([
	            'user_id' => $seller->id,
	            'type_id' => 5,
	            'short_text' => 'Сделка #'.$deal->id.' отменена гарантом в Вашу пользу.',
	            'full_text' => 'Сделка #'.$deal->id.' отменена гарантом в Вашу пользу. Средства переведены на Вас счет.'
	        ]);

        	$message_text = 'Администрация одобрила заявку на отмену сделки в пользу продавца.
        	Сделка завершена.
        	Продавцу начислено <b><span style="color: #0ad251;">'.$advert->price.' руб.</span></b>';
        } else {
        	$buyer = User::find($deal->user_id);
	        $buyer->balance = $buyer->balance + $advert->price;
	        $buyer->save();

	        Cost::create([
	            'user_id' => $buyer->id,
	            'type' => 1,
	            'text' => 'Отмененная гарантом сделка #'.$deal->id,
	            'amount' => $advert->price
	        ]);

	        Notice::create([
	            'user_id' => $buyer->id,
	            'type_id' => 5,
	            'short_text' => 'Сделка #'.$deal->id.' отменена гарантом в Вашу пользу.',
	            'full_text' => 'Сделка #'.$deal->id.' отменена гарантом в Вашу пользу. Средства переведены на Вас счет.'
	        ]);

	        $message_text = 'Администрация одобрила заявку на отмену сделки в пользу покупателя.
        	Сделка завершена.
        	Покупателю начислено <b><span style="color: #0ad251;">'.$advert->price.' руб.</span></b>';
        }

        $message = Message::send($deal->user_id, $advert->user_id, $message_text, 1, 0);

        DB::table('dialog_members')->where('dialog_id', $message->dialog_id)->where('role', 1)->delete();

		return redirect()->route('admin.cancelreq')->with('success', 'Заявка одобрена.');
	}
}