<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Wall;
use App\Like;
use App\User;
use App\Notice;

class WallController extends Controller
{
    public function view($post_id){
        $post = Wall::findOrFail($post_id);
        $author = User::findOrFail($post->author_id);
        $auth_user = Auth::user();

        return view('wall.view', [
            'title' => $author->getFullName().' / Пост #'.$post->id,
            'post' => $post,
            'author' => $author,
            'auth_user' => $auth_user
        ]);
    }

    public function get($post_id){
        $post = Wall::findOrFail($post_id);

        $likes = Like::where('post_id', $post->id)->orderBy('id', 'DESC')->get();
        $count = count($likes);

        foreach($likes as $like){
            $like['user'] = User::find($like->author_id);
            $like['user']['full_name'] = $like['user']->getFullName();
            $like['user']['profile_url'] = route('users.view', ['user_id' => $like->author_id]);
        }

        return json_encode(['success' => true, 'likes' => $likes, 'count' => $count]);
    }

    public function getComment($comment_id){
        $comment = Comment::findOrFail($comment_id);

        $likes = Like::where('comment_id', $comment->id)->orderBy('id', 'DESC')->get();
        $count = count($likes);

        foreach($likes as $like){
            $like['user'] = User::find($like->author_id);
            $like['user']['full_name'] = $like['user']->getFullName();
            $like['user']['profile_url'] = route('users.view', ['user_id' => $like->author_id]);
        }

        return json_encode(['success' => true, 'likes' => $likes, 'count' => $count]);
    }

    public function post(Request $request, $user_id){
        $user = Auth::user();
        if($user->id != $user_id) return abort(404);

        $rules = [
            'text' => 'required|string'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->route('users.view', ['user_id' => $user->id])->withErrors($validator);
        }

        if($wall = Wall::create([
            'author_id' => $user->id,
            'text' => $request->input('text')
        ])){
            return redirect()->route('users.view', ['user_id' => $user->id])->with('success', 'Опубликовано.');
        } else return redirect()->route('users.view', ['user_id' => $user->id])->withErrors(['Что-то пошло не так. Попробуйте позже.']);
    }

    public function postComment(Request $request, $post_id){
        $post = Wall::findOrFail($post_id);
        $user = Auth::user();

        $rules = [
            'text' => 'required|string'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->route('users.view', ['user_id' => $user->id])->withErrors($validator);
        }

        if($comment = Comment::create([
            'post_id' => $post->id,
            'author_id' => $user->id,
            'text' => $request->input('text')
        ])){
            if($post->author_id != $user->id) Notice::create([
                'user_id' => $post->author_id,
                'type_id' => 3,
                'short_text' => sprintf('%s прокомментировал Вашу публикацию.', $user->getFullName()),
                'full_text' => sprintf('<a href="%s">%s</a> прокомментировал <a href="%s">Вашу публикацию</a>.', route('users.view', ['user_id' => $user->id]), $user->getFullName(), route('wall.view', ['post_id' => $post->id]).'#comment'.$comment->id)
            ]);
            return redirect(route('wall.view', ['post_id' => $post->id]).'#comment'.$comment->id);
        } else return redirect()->route('users.view', ['user_id' => $user->id])->withErrors(['Что-то пошло не так. Попробуйте позже.']);
    }

    public function like($post_id){
        $post = Wall::findOrFail($post_id);
        $user = Auth::user();

        if($like = $post->isUserLike($user->id)){
            $like->delete();

            return json_encode(['success' => true, 'likes' => $post->getLikes(), 'is_liked' => 0]);
        } else {
            Like::create([
                'post_id' => $post->id,
                'author_id' => $user->id
            ]);

            if($post->author_id != $user->id) Notice::create([
                'user_id' => $post->author_id,
                'type_id' => 2,
                'short_text' => sprintf('%s оценил Вашу публикацию.', $user->getFullName()),
                'full_text' => sprintf('<a href="%s">%s</a> оценил <a href="%s">Вашу публикацию</a>.', route('users.view', ['user_id' => $user->id]), $user->getFullName(), route('users.view', ['user_id' => $post->author_id]).'#post'.$post->id)
            ]);


            return json_encode(['success' => true, 'likes' => $post->getLikes(), 'is_liked' => 1]);
        }
    }

    public function commentLike($comment_id){
        $comment = Comment::findOrFail($comment_id);
        $user = Auth::user();

        if($like = $comment->isUserLike($user->id)){
            $like->delete();

            return json_encode(['success' => true, 'likes' => $comment->getLikes(), 'is_liked' => 0]);
        } else {
            Like::create([
                'comment_id' => $comment->id,
                'author_id' => $user->id
            ]);

            if($comment->author_id != $user->id) Notice::create([
                'user_id' => $comment->author_id,
                'type_id' => 4,
                'short_text' => sprintf('%s оценил Ваш комментарий.', $user->getFullName()),
                'full_text' => sprintf('<a href="%s">%s</a> оценил <a href="%s">Ваш комментарий</a>.', route('users.view', ['user_id' => $user->id]), $user->getFullName(), route('wall.view', ['post_id' => $comment->post_id]).'#comment'.$comment->id)
            ]);


            return json_encode(['success' => true, 'likes' => $comment->getLikes(), 'is_liked' => 1]);
        }
    }

    // edit & delete
    public function edit(Request $request, $post_id){
        $auth = Auth::user();
        $post = Wall::findOrFail($post_id);

        if($auth->id != $post->author_id) return abort(404);

        if($request->isMethod('post')){
            $rules = [
                'text' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                return redirect()->route('wall.edit', ['post_id' => $post->id])->withErrors($validator);
            }

            $post->text = $request->input('text');

            if($post->save()) return redirect()->route('wall.view', ['post_id' => $post->id])->with('success', 'Сохранено.');
            else return redirect()->route('wall.edit', ['post_id' => $post->id])->withErrors(['Что-то пошло не так. Попробуйте позже.']);
        } else {
            return view('wall.edit', [
                'title' => 'Редактирование поста',
                'post' => $post
            ]);
        }
    }

    public function delete($post_id){
        $auth = Auth::user();
        $post = Wall::findOrFail($post_id);

        if($auth->id != $post->author_id) return abort(404);

        $post->delete();

        return redirect()->route('users.view', ['user_id' => $auth->id])->withErrors(['Пост удален.']);
    }

    public function commentEdit(Request $request, $comment_id){
        $auth = Auth::user();
        $comment = Comment::findOrFail($comment_id);

        if($auth->id != $comment->author_id) return abort(404);

        if($request->isMethod('post')){
            $rules = [
                'text' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                return redirect()->route('comment.edit', ['comment_id' => $comment->id])->withErrors($validator);
            }

            $comment->text = $request->input('text');

            if($comment->save()) return redirect()->route('wall.view', ['post_id' => $comment->post_id])->with('success', 'Сохранено.');
            else return redirect()->route('comment.edit', ['comment_id' => $comment->id])->withErrors(['Что-то пошло не так. Попробуйте позже.']);
        } else {
            return view('wall.comment-edit', [
                'title' => 'Редактирование комментария',
                'comment' => $comment
            ]);
        }
    }

    public function commentDelete($comment_id){
        $auth = Auth::user();
        $comment = Comment::findOrFail($comment_id);

        if($auth->id != $comment->author_id) return abort(404);

        $comment->delete();

        return redirect()->route('wall.view', ['post_id' => $comment->post_id])->withErrors(['Комментарий удален.']);
    }
}
