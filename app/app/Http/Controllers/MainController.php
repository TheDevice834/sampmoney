<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Advert;
use App\Activity;

class MainController extends Controller
{
    public function index()
    {
        $adverts = Advert::orderBy('id', 'DESC')->where('is_hide', 0)->limit(10)->get();

        $activity = [];
        foreach(Activity::orderBy('id', 'DESC')->limit(10)->get() as $item){
            $activity[] = [
                'activity' => $item,
                'user' => User::find($item->user_id)
            ];
        }

    	$adverts_count = Advert::count();

        $garants = User::where('garant', 1)->where('last_activity', '>', (time() - 600))->get();

        return view('main.index', [
        	'adverts' => $adverts,
        	'adverts_count' => $adverts_count,
            'activity' => $activity,
            'garants' => $garants
    	]);
    }
}
