<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advert;
use App\Deal;
use App\Message;
use Auth;
use DB;

class GarantController extends Controller
{
    public function index(){
        $deals = Deal::where('garant', 0)->orderBy('id', 'DESC')->paginate(20);

    	return view('garant.index', [
    		'title' => 'Панель гаранта',
            'deals' => $deals
    	]);
    }

    public function call($deal_id){
        $auth = Auth::user();
        $deal = Deal::findOrFail($deal_id);
        $advert = Advert::findOrFail($deal->advert_id);

        if($auth->id != $deal->user_id && $auth->id != $advert->user_id) return abort(403);

        if(isset($deal->garant)) return abort(404);

        $deal->garant = 0;
        $deal->save();

        $message_text = $auth->getFullName().' вызвал гаранта.
        Ожидайте.';
        $message = Message::send($deal->user_id, $advert->user_id, $message_text, 1, 0);

        return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);
    }

    public function accept($deal_id){
        $auth = Auth::user();
        $deal = Deal::findOrFail($deal_id);
        $advert = Advert::findOrFail($deal->advert_id);

        if(isset($deal->garant) && $deal->garant != 0) return abort(404);

        $deal->garant = $auth->id;
        $deal->save();

        $message_text = '<a href="'.route('users.view', ['user_id' => $auth->id]).'" target="_blank">'.$auth->getFullName().'</a> присоединился к беседе в качестве гаранта.';
        $message = Message::send($deal->user_id, $advert->user_id, $message_text, 1, 0);

        DB::table('dialog_members')->insert([
            'dialog_id' => $message->dialog_id,
            'user_id' => $auth->id,
            'timestamp' => time(),
            'role' => 1
        ]);

        return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);
    }

    public function cancel(Request $request, $deal_id){
        $auth = Auth::user();
        $deal = Deal::findOrFail($deal_id);
        $advert = Advert::findOrFail($deal->advert_id);

        if(!isset($deal->garant) || isset($deal->garant) && $deal->garant != $auth->id) return abort(403);

        if(!$request->filled('in_favor')) return abort(404);
        $in_favor = intval($request->input('in_favor'));
        if($in_favor != 1 && $in_favor != 2) return abort(404);

        if(DB::table('deal_cancelreq')->where('deal_id', $deal->id)->where('garant_id', $auth->id)->where('status', 0)->first()) return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);

        DB::table('deal_cancelreq')->insert([
            'deal_id' => $deal->id,
            'garant_id' => $auth->id,
            'in_favor' => $in_favor,
            'timestamp' => time()
        ]);

        $message_text = 'Гарант послал запрос на отмену сделки ';
        if($in_favor == 1) $message_text .= 'в пользу продавца.
        Ожидайте.';
        else $message_text .= 'в пользу покупателя.
        Ожидайте.';
        
        $message = Message::send($deal->user_id, $advert->user_id, $message_text, 1, 0);

        return redirect()->route('deal.redirect', ['deal_id' => $deal->id]);
    }
}
