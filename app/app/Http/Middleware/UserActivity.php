<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user = Auth::user();

            $user->last_activity = time();
            $user->save();
        }

        return $next($request);
    }
}
