<?php

namespace App\Http\Middleware;

use Closure;
use App\Admin;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user = Auth::user();

            if(!$user->isAdmin()){
                return abort(403);
            }
        }

        return $next($request);
    }
}
