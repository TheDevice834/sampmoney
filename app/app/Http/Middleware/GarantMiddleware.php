<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class GarantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user = Auth::user();

            if($user->garant == 0){
                return abort(403);
            }
        }

        return $next($request);
    }
}
