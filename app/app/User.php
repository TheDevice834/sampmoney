<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vk_id', 'first_name', 'last_name', 'email', 'avatar', 'access_token', 'expires_in', 'show_vk_link', 'last_activity', 'balance', 'garant'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function getFullName(){
        if($this->show_name == 1) return $this->first_name.' '.$this->last_name;
        else {
            return 'Anonymous'.$this->id;
        }
    }

    public function getNotices($is_read = 0, $limit = 5){
        return Notice::where('user_id', $this->id)->where('is_read', $is_read)->orderBy('id', 'DESC')->limit($limit)->get();
    }

    public function getWall(){
        return Wall::where('author_id', $this->id)->orderBy('id', 'DESC')->get();
    }

    public function getUnreadMessages(){
        $unread_messages = [];

        $dialogs = DB::table('dialog_members')->where('user_id', $this->id)->get();

        foreach($dialogs as $dialog){
            foreach(Message::where('dialog_id', $dialog->dialog_id)->where('is_read', 0)->get() as $message){
                if($this->id != $message->sender_id) $unread_messages[$message->id] = [
                    'message' => $message
                ];
            }
        }

        return $unread_messages;
    }

    public function getOnline(){
        $online_interval = 600;
        $status = '';

        if(($this->last_activity + $online_interval) > time()) $status = 'онлайн';
        else {
            $status = 'был активен '.dateConvert($this->last_activity);
        }

        return $status;
    }

    public function isAdmin(){
        if($admin = Admin::where('user_id', $this->id)->first()) return true;
        else return false;
    }

    public function isGarant(){
        if($this->garant == 1) return true;
        else return false;
    }

    public function getAdverts(){
        return Advert::where('user_id', $this->id)->orderBy('id', 'DESC')->get();
    }
}
