<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'access_level', 'author_id', 'title', 'text', 'read_count'
    ];

    public function getAccessLevelTitle(){
    	$access_level = [
    		0 => 'Видна всем',
    		1 => 'Видна только авторизированным пользователям',
    		2 => 'Видна только гостям',
    	];

    	return $access_level[$this->access_level];
    }

    public function makeRead(){
        $this->read_count++;
        $this->save();
    }
}
