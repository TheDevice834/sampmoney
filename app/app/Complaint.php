<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = [
        'author_id', 'culprit_id', 'cause', 'object', 'object_id'
    ];

    public static function getCauses(){
        return [
            1 => 'Неактуальное объявление',
            2 => 'Оскорбительное поведение автора',
            3 => 'Реклама',
        ];
    }

    public static function getObjects(){
        return [
            1 => 'Wall',
            2 => 'Comment',
            3 => 'Advert'
        ];
    }
}
