<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Message extends Model
{
    protected $fillable = [
        'dialog_id', 'sender_id', 'destination_id', 'system_id', 'message', 'is_read', 'category'
    ];

    public function getCategory($category = null){
    	$categories = [
    		2 => [
    			'title' => 'Новое',
    			'name' => 'new',
    			'color' => 'success',
    		], 3 => [
    			'title' => 'Оплачено',
    			'name' => 'paid',
    			'color' => 'primary',
    		], 4 => [
    			'title' => 'От гаранта',
    			'name' => 'guarantee',
    			'color' => 'warning',
    		], 5 => [
    			'title' => 'Важное',
    			'name' => 'important',
    			'color' => 'danger',
    		],
    	];

    	if(isset($category)) return $categories[$category];
    	else return $categories;
    }

    public static function getSystems($system_id){
        $systems = [
            1 => [
                'title' => 'Сделка',
                'avatar' => route('home').'/assets/img/deal.png'
            ]
        ];

        return $systems[$system_id];
    }

    public static function findDialog($sender_id, $recipient_id){
        $sender = User::findOrFail($sender_id);
        $recipient = User::findOrFail($recipient_id);

        $dialog = [];

        foreach(DB::table('dialog_members')->where('user_id', $sender->id)->get() as $senderDialog){
            if($member = DB::table('dialog_members')->where('dialog_id', $senderDialog->dialog_id)->where('user_id', $recipient->id)->first()){
                if($senderDialog->id != $member->id){
                    $dialog = Dialog::find($senderDialog->dialog_id);
                    break;
                }
            }
        }

        if(!empty($dialog)) return $dialog;
        else return false;
    }

    public static function send($sender_id, $recipient_id, $message, $system_id = NULL, $destination_id = NULL, $dialog = NULL){
        $sender = User::findOrFail($sender_id);
        $recipient = User::findOrFail($recipient_id);

        if(isset($dialog) && $member = DB::table('dialog_members')->where('user_id', $sender->id)->where('dialog_id', $dialog)->first()){
            $dialog_exists = true;
            $dialog = Dialog::find($member->dialog_id);
        } else {
            $dialog_exists = false;
            $dialog = 0;

            foreach(DB::table('dialog_members')->where('user_id', $sender->id)->where('role', 0)->get() as $senderDialog){
                if($member = DB::table('dialog_members')->where('dialog_id', $senderDialog->dialog_id)->where('user_id', $recipient->id)->first()){
                    if($senderDialog->id != $member->id){
                        $dialog_exists = true;
                        $dialog = Dialog::find($senderDialog->dialog_id);
                        break;
                    }
                }
            }
        }

        if(!$dialog_exists){
            $dialog = Dialog::create([]);

            DB::table('dialog_members')->insert([
                'dialog_id' => $dialog->id,
                'user_id' => $sender->id,
                'timestamp' => time()
            ]);

            DB::table('dialog_members')->insert([
                'dialog_id' => $dialog->id,
                'user_id' => $recipient->id,
                'timestamp' => time()
            ]);
        }

        if(!isset($system_id)) $message = Message::create([
            'dialog_id' => $dialog->id,
            'sender_id' => $sender->id,
            'message' => $message
        ]);
        else {
            $message = Message::create([
                'dialog_id' => $dialog->id,
                'message' => $message,
                'system_id' => $system_id,
                'destination_id' => $destination_id
            ]);
        }

        return $message;
    }
}
