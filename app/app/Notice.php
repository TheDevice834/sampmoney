<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $fillable = [
        'user_id', 'type_id', 'short_text', 'full_text', 'is_read'
    ];

    public function getTypeInfo(){
        switch($this->type_id){
            case 0:
                $icon = 'icon-plus';
                $title = 'Добро пожаловать';
                break;
            case 1:
                $icon = 'icon-user-alt-add-2';
                $title = 'Новый подписчик';
                break;
            case 2:
                $icon = 'icon-heart-full';
                $title = 'Публикация';
                break;
            case 3:
                $icon = 'icon-speech-bubble-left-tip-text';
                $title = 'Публикация';
                break;
            case 4:
                $icon = 'icon-heart-full';
                $title = 'Комментарий';
                break;
            case 5:
                $icon = 'icon-credit-card-alt-3';
                $title = 'Сделка';
                break;
            case 6:
                $icon = 'icon-clipboard-alt';
                $title = 'Объявление';
                break;
        }

        return [
            'icon' => $icon,
            'title' => $title
        ];
    }

    public function makeRead(){
        $this->is_read = 1;
        $this->save();
    }
}
