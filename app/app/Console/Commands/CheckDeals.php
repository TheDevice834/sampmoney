<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Deal;
use App\Message;
use App\Advert;
use App\Notice;
use App\Cost;

class CheckDeals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking the relevance of the transaction.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deals = Deal::get();

        foreach($deals as $deal){
            if($deal->status == 4) continue;
            if($deal->status == 1 && ($deal->created_at->timestamp + 3600) < time()){
                $deal->delete();

                if($advert = Advert::find($deal->advert_id)){
                    $message = Message::send($deal->user_id, $advert->user_id, 'Сделка больше неактуальна.', 1, $deal->user_id);

                    $advert->is_hide = 0;
                    $advert->save();

                    Notice::create([
                        'user_id' => $deal->user_id,
                        'type_id' => 5,
                        'short_text' => 'Сделка #'.$deal->id.' больше неактуальна.',
                        'full_text' => 'Сделка #'.$deal->id.' больше неактуальна.'
                    ]);
                }
            } elseif($deal->status != 1 && ($deal->updated_at->timestamp + 86400) < time()){
                $deal->delete();

                if($advert = Advert::find($deal->advert_id)){
                    $message = Message::send($deal->user_id, $advert->user_id, 'Сделка больше неактуальна. Средства переведены на счет продавца.', 1, 0);

                    $advert->is_hide = 0;
                    $advert->save();

                    if($user = User::find($advert->user_id)){
                        $user->balance = $user->balance + $advert->price;
                        $user->save();

                        Cost::create([
                            'user_id' => $user->id,
                            'type' => 1,
                            'text' => 'Прерванная (неактуальная) сделка #'.$deal->id,
                            'amount' => $advert->price
                        ]);
                    }

                    Notice::create([
                        'user_id' => $deal->user_id,
                        'type_id' => 5,
                        'short_text' => 'Сделка #'.$deal->id.' больше неактуальна.',
                        'full_text' => 'Сделка #'.$deal->id.' больше неактуальна.'
                    ]);

                    Notice::create([
                        'user_id' => $advert->user_id,
                        'type_id' => 5,
                        'short_text' => 'Сделка #'.$deal->id.' больше неактуальна.',
                        'full_text' => 'Сделка #'.$deal->id.' больше неактуальна.'
                    ]);
                }
            }
        }
    }
}
