<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Dialog extends Model
{
    protected $fillable = [

    ];

    public static function getDialog($dialog_id, $user_id = null){
        $dialog = self::findOrFail($dialog_id);

        $members = [];
        $garant = [];
        $is_garant = false;

        if(!$is_member = DB::table('dialog_members')->where('dialog_id', $dialog_id)->where('user_id', $user_id)->first()){
            return ['security' => false];
        } else {
            if($is_member->role == 1) $is_garant = true;
        }

        foreach(DB::table('dialog_members')->where('dialog_id', $dialog_id)->where('role', 0)->get() as $member){
            if(isset($user_id)){
                if($member->user_id != $user_id){
                    $members = User::find($member->user_id);
                    break;
                }
            } else {
                $user = User::find($member->user_id);
                $members[$user->id] = $user;
            }
        }

        if(empty($members)){
            if(DB::table('dialog_members')->where('dialog_id', $dialog_id)->where('user_id', $user_id)->count() > 1){
                $members = User::find($user_id);
            }
        }

        if($member = DB::table('dialog_members')->where('dialog_id', $dialog_id)->where('role', 1)->first()){
            $garant = User::find($member->user_id);
        }

        $messages = [];

        foreach(Message::where('dialog_id', $dialog_id)->get() as $message){
            $messages[$message->id] = $message;
        }

        ksort($messages, SORT_NUMERIC);

        return [
            'members' => $members,
            'messages' => $messages,
            'garant' => $garant,
            'is_garant' => $is_garant
        ];
    }

    public static function getDialogList($user_id){
        $dialog_ids = DB::table('dialog_members')->where('user_id', $user_id)->orderBy('id', 'DESC')->get();

        $dialogs = [];
        foreach($dialog_ids as $dialog_id){
            $is_garant = false;
            $dialog_members = [];

            foreach(DB::table('dialog_members')->where('dialog_id', $dialog_id->dialog_id)->get() as $member){
                if($dialog_id->id != $member->id){
                    $dialog_members[] = [
                        'user' => User::findOrFail($member->user_id),
                    ];
                } else {
                    if($member->user_id == $user_id && $member->role == 1) $is_garant = true;
                }
                
            }

            $last_message = Message::where('dialog_id', $dialog_id->dialog_id)->orderBy('id', 'DESC')->first();

            if(isset($last_message->destination_id) && $is_garant == false){
                if($last_message->destination_id != $user_id && $last_message->destination_id != 0){
                    $last_message = Message::where('dialog_id', $dialog_id->dialog_id)
                        ->where(function($query) use ($user_id){
                            $query->where('destination_id', 0)
                                  ->orWhere('destination_id', $user_id);
                        })
                        ->orderBy('id', 'DESC')
                        ->first();
                }
            }

            $dialogs[$last_message->id] = [
                'last_message' => $last_message,
                'dialog_id' => $dialog_id->dialog_id,
                'dialog_members' => $dialog_members,
                'is_garant' => $is_garant
            ];
        }

        krsort($dialogs, SORT_NUMERIC);

        return $dialogs;
    }
}