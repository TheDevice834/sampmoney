<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'post_id', 'comment_id', 'author_id', 'text'
    ];

    public function getAuthor(){
        return User::find($this->author_id);
    }

    public function isUserLike($user_id){
        if($like = Like::where('comment_id', $this->id)->where('author_id', $user_id)->first()) return $like;
        else return false;
    }

    public function getLikes(){
        return Like::where('comment_id', $this->id)->count();
    }
}
