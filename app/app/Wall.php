<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wall extends Model
{
    protected $table = 'wall';

    protected $fillable = [
        'author_id', 'text', 'attachments',
    ];

    public function getLikes(){
        return Like::where('post_id', $this->id)->count();
    }

    public function getCommentsCount(){
        return Comment::where('post_id', $this->id)->count();
    }

    public function getComments(){
        return Comment::where('post_id', $this->id)->get();
    }

    public function isUserLike($user_id){
        if($like = Like::where('post_id', $this->id)->where('author_id', $user_id)->first()) return $like;
        else return false;
    }
}
