<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $fillable = [
        'user_id', 'advert_id', 'status', 'garant'
    ];

    public function getStatus(){
    	$status = [
    		1 => 'Ожидается оплата',
            2 => 'Ожидается подтверждение продавца об успешной передаче виртов',
            3 => 'Ожидается подтверждение покупателя об успешном получении виртов',
            4 => 'Завершена'
        ];

    	return $status[$this->status];
    }
}
