<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Advert extends Model
{
    protected $fillable = [
        'user_id', 'type', 'server', 'server_name', 'amount', 'transfer_method', 'price', 'is_hide'
    ];

    public function getServerName(){
        if(isset($this->server)) return DB::table('servers')->find($this->server)->name;
        else return $this->server_name;
    }

    public function getUser(){
        if(!isset($this->user)){
            $this->user = User::find($this->user_id);
        }

        return $this->user;
    }

    public static function getTypes(){
        return [
            1 => 'Покупка',
            2 => 'Продажа',
        ];
    }

    public static function getTransferMethods(){
    	return [
    		1 => 'Банковский перевод',
    		2 => 'Вместе с аккаунтом',
    		3 => 'Через /pay',
    		4 => 'Через дом\бизнес',
    		5 => 'Через симкарту',
    	];
    }

    public static function getDefaultServers(){
        return [
            [
                'name' => 'Diamond Role Play | Emerald',
                'ip' => '5.254.123.2:7777',
            ], [
                'name' => 'Diamond Role Play | Radiant',
                'ip' => '5.254.123.3:7777',
            ], [
                'name' => 'Diamond Role Play | Trilliant',
                'ip' => '5.254.123.4:7777',
            ], [
                'name' => 'Diamond Role Play | Crystal',
                'ip' => '5.254.123.5:7777',
            ], [
                'name' => 'Diamond Role Play | Sapphire',
                'ip' => '5.254.123.6:7777',
            ], [
                'name' => 'Diamond Role Play | Onyx',
                'ip' => '5.254.105.202:7777',
            ], [
                'name' => 'Diamond Role Play | Amber',
                'ip' => '5.254.105.203:7777',
            ], [
                'name' => 'Diamond Role Play | Quartz',
                'ip' => '5.254.105.204:7777',
            ], [
                'name' => 'Arizona Role Play | Phoenix',
                'ip' => '185.169.134.3:7777',
            ], [
                'name' => 'Arizona Role Play | Tucson',
                'ip' => '185.169.134.4:7777',
            ], [
                'name' => 'Arizona Role Play | Scottdale',
                'ip' => '185.169.134.43:7777',
            ], [
                'name' => 'Arizona Role Play | Chandler',
                'ip' => '185.169.134.44:7777',
            ], [
                'name' => 'Arizona Role Play | Brainburg',
                'ip' => '185.169.134.45:7777',
            ], [
                'name' => 'Arizona Role Play | Saint Rose',
                'ip' => '185.169.134.5:7777',
            ], [
                'name' => 'Arizona Role Play | Mesa',
                'ip' => '185.169.134.59:7777',
            ], [
                'name' => 'Arizona Role Play | Red-Rock',
                'ip' => '185.169.134.61:7777',
            ]
        ];
    }
}
