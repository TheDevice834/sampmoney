<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    protected $fillable = [
        'user_id', 'type', 'text', 'amount'
    ];

    public function showType(){
    	$types = [
    		1 => 'Пополнение',
    		2 => 'Расходы'
    	];

    	return $types[$this->type];
    }
}
