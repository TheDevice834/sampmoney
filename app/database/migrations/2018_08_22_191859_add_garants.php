<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGarants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dialog_members', function (Blueprint $table) {
            $table->integer('role')->after('user_id')->default(0);
        });

        Schema::table('deals', function (Blueprint $table) {
            $table->integer('garant')->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dialog_members', function (Blueprint $table) {
            $table->dropColumn('role');
        });

        Schema::table('deals', function (Blueprint $table) {
            $table->dropColumn('garant');
        });
    }
}
