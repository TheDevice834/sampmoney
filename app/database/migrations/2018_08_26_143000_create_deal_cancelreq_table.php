<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealCancelreqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_cancelreq', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deal_id');
            $table->integer('garant_id');
            $table->integer('in_favor');
            $table->integer('status')->default(0);
            $table->integer('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_cancelreq');
    }
}
