<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->integer('sender_id')->nullable()->change();
            $table->integer('system_id')->after('sender_id')->nullable();
            $table->integer('destination_id')->after('system_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->integer('sender_id')->nullable(false)->change();
            $table->dropColumn('system_id');
            $table->dropColumn('destination_id');
        });
    }
}
