<?php

use Faker\Generator as Faker;

$factory->define(App\Notice::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'type_id' => 0,
        'text' => 'Регистрация успешно завершена.'
    ];
});
