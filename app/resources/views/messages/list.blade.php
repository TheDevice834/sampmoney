@extends('app')

@section('body')
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Центр сообщений
                </div>
                <div class="card-mailbox">
                    <div class="row mx-0">
                        <div class="col-lg-3 pt-5">
                            <div class="mailbox-menu">
                                <div class="mailbox-compose-outer">
                                    <div class="compose-message">
                                        <a href="{{ route('messages.send') }}" class="btn btn-primary btn-block">
                                            Написать сообщение
                                        </a>
                                    </div>
                                </div>
                                <div class="mailbox-tags">
                                    <h5>Категории</h5>
                                    <a href="#">
                                        <span class="badge badge-success">Новые</span>
                                    </a>
                                    <a href="#">
                                        <span class="badge badge-primary">Оплачено</span>
                                    </a>
                                    <a href="#">
                                        <span class="badge badge-warning">От гаранта</span>
                                    </a>
                                    <a href="#">
                                        <span class="badge badge-danger">Важное</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="mailbox-container mailbox-email-list">
                                @if(count($dialogs) > 0)
                                    <table class="table table-hover">
                                        @foreach($dialogs as $dialog)
                                            @php
                                                $hideDialog = false;
                                                if(count($dialog['dialog_members']) == 2 || count($dialog['dialog_members']) == 1){
                                                    $dialog_title = $dialog['dialog_members'][0]['user']->getFullName();
                                                    $dialog_url = route('users.view', ['user_id' => $dialog['dialog_members'][0]['user']->id]);
                                                    $dialog_image = $dialog['dialog_members'][0]['user']->avatar;
                                                    $dialog_online = $dialog['dialog_members'][0]['user']->getOnline();
                                                }

                                                if(isset($dialog['last_message']->system_id) && $dialog['is_garant'] == false){
                                                    if($dialog['last_message']->destination_id != $user->id && $dialog['last_message']->destination_id != 0) $hideDialog = true;
                                                }
                                            @endphp
                                            @if($hideDialog == false)
                                                <tr @if($dialog['last_message']->is_read == 0) class="email-status-unread" @endif style="border: #eeeef4 solid 1px;" onclick="goToDialog('{{ route('messages.dialog', ['dialog_id' => $dialog['dialog_id']]) }}')">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                @if($dialog['is_garant'] == true)
                                                                    <img src="{{ App\Message::getSystems(1)['avatar'] }}" width="44" height="44" style="float: left;margin-right: 15px;border-radius: 50%;">
                                                                    <span style="color: #f43a59;"><b>Вы гарант в данном диалоге</b></span>
                                                                @else
                                                                    <img src="{{ $dialog_image }}" width="44" height="44" style="float: left;margin-right: 15px;border-radius: 50%;">
                                                                    Диалог с <a href="{{ $dialog_url }}" target="_blank">{{ $dialog_title }}</a> ({{ $dialog_online }})
                                                                @endif
                                                                <br>
                                                                @if(isset($dialog['last_message']->system_id))
                                                                    {!! str_limit($dialog['last_message']->message, 45, '...') !!}
                                                                @else
                                                                    @if($dialog['last_message']->sender_id == Auth::user()->id)Вы: @endif{{ $dialog['last_message']->message }}
                                                                @endif
                                                                <span style="float: right;">{{ dateConvert($dialog['last_message']->created_at->timestamp) }}</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function goToDialog(url){
            window.location.href = url;
        }
    </script>
@endsection
