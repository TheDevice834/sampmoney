@extends('app')

@section('body')
    <div class="row mb-5">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="{{ route('messages') }}" class="btn btn-primary btn-block">Вернуться к сообщениям</a>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ $title }}
                    <p class="task-list-stats">
                        @if(!empty($deal) && $dialog['is_garant'] == true)
                            Последния изменения статуса сделки: {{ dateConvert($deal['deal']->updated_at->timestamp) }}
                        @else
                            {{ $dialog['members']->getOnline() }}
                        @endif
                    </p>
                </div>
                <div class="card-body">
                    @if(!empty($deal))
                        @if($deal['deal']->status == 1 && $deal['deal']->user_id != $user->id)
                        @else
                            <div class="alert alert-info" style="margin-bottom: 30px;">
                                Сделка <b>#{{ $deal['deal']->id }}</b><br>
                                Объявление: <a href="{{ route('adverts.view', ['advert_id' => $deal['advert']->id]) }}" target="_blank"><b>Перейти</b></a><br>
                                Способ передачи виртов: <b>{{ $deal['advert']->getTransferMethods()[$deal['advert']->transfer_method] }}</b><br>
                                Статус: <b>{{ $deal['deal']->getStatus() }}</b>
                                @if(isset($deal['deal']->garant))
                                    <br>
                                    <br>
                                    @if($deal['deal']->garant == 0)
                                        Гарант: <b>ожидается...</b>
                                    @else
                                        Гарант: <a href="{{ route('users.view', ['user_id' => $dialog['garant']->id]) }}" target="_blank"><b>{{ $dialog['garant']->getFullName() }}</b></a>
                                    @endif
                                @endif
                                <br><br>
                                @if($deal['deal']->status != 1)
                                    @if($deal['seller']->id == $user->id && $deal['deal']->status == 2)
                                        <a href="{{ route('deal.transfer', ['deal_id' => $deal['deal']->id]) }}" class="btn btn-success btn-sm">Я передал товар</a>
                                    @elseif($deal['deal']->user_id == $user->id && $deal['deal']->status == 3)
                                        <a href="{{ route('deal.transfer', ['deal_id' => $deal['deal']->id]) }}" class="btn btn-success btn-sm">Я получил товар</a>
                                    @endif
                                    @if(!$dialog['is_garant'])
                                        @if(!isset($deal['deal']->garant))
                                            <a href="{{ route('garant.call', ['deal_id' => $deal['deal']->id]) }}" class="btn btn-dark btn-sm">Вызвать живого гаранта</a>
                                        @else
                                            <a class="btn btn-dark btn-sm disabled">Вызвать живого гаранта</a>
                                        @endif
                                    @else
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Отменить сделку
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{ route('garant.cancel', ['deal_id' => $deal['deal']->id]) }}?in_favor=1">В пользу продавца</a>
                                                <a class="dropdown-item" href="{{ route('garant.cancel', ['deal_id' => $deal['deal']->id]) }}?in_favor=2">В пользу покупателя</a>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <a href="{{ route('deal.cancel', ['deal_id' => $deal['deal']->id]) }}" class="btn btn-danger btn-sm">Отменить сделку</a>
                                @endif
                                <a class="btn btn-info btn-sm" style="float:right;" data-toggle="modal" data-target="#howToModal">Как проходит сделка?</a>
                            </div>
                        @endif
                    @endif
                    <div id="messagebox" style="position: relative;overflow: auto;overflow-x: hidden;width: 100%;height: 350px;">
                        <table class="table table-hover">
                            @foreach($dialog['messages'] as $message)
                                @php
                                    $hideMessage = false;

                                    if(!isset($message->system_id)){
                                        if($message->sender_id == $user->id) $sender_info = [
                                            'id' => $user->id,
                                            'full_name' => $user->getFullName(),
                                            'avatar' => $user->avatar,
                                        ];
                                        // elseif(!empty($dialog['garant']) && $message->sender_id == $dialog['garant']->id){
                                        //     $sender_info = [
                                        //         'id' => $dialog['garant']->id,
                                        //         'full_name' => $dialog['garant']->getFullName(),
                                        //         'avatar' => $dialog['garant']->avatar,
                                        //     ];
                                        // }
                                        // else $sender_info = [
                                        //     'id' => $dialog['members']->id,
                                        //     'full_name' => $dialog['members']->getFullName(),
                                        //     'avatar' => $dialog['members']->avatar,
                                        // ];

                                        // if($message->sender_id != $sender_info['id']){
                                        //     $sender = App\User::find($message->sender_id);
                                        //     $sender_info = [
                                        //         'id' => $sender->id,
                                        //         'full_name' => $sender->getFullName(),
                                        //         'avatar' => $sender->avatar,
                                        //     ];
                                        // }
                                        else {
                                            $sender = App\User::find($message->sender_id);
                                            $sender_info = [
                                                'id' => $sender->id,
                                                'full_name' => $sender->getFullName(),
                                                'avatar' => $sender->avatar,
                                            ];
                                        }
                                    } else {
                                        if($message->destination_id != $user->id && $message->destination_id != 0 && $dialog['is_garant'] == false) $hideMessage = true;

                                        $sender_info = [
                                            'full_name' => App\Message::getSystems($message->system_id)['title'],
                                            'avatar' => App\Message::getSystems($message->system_id)['avatar'],
                                            'show_html' => true
                                        ];
                                    }
                                @endphp
                                @if($hideMessage == false)
                                    <tr @if($message->is_read == 0) style="background: #ededed;" @endif>
                                        <td style="border: #eeeef4 solid 1px;">
                                            <div class="row">
                                                <div class="col-md-12" style="font-weight: 500;">
                                                    <img src="{{ $sender_info['avatar'] }}" width="44" height="44" style="float: left;border-radius: 50%;">
                                                    <div style="padding-left: 55px;">
                                                        @if(isset($sender_info['id']))
                                                            <a href="{{ route('users.view', ['user_id' => $sender_info['id']]) }}" target="_blank"><b>{{ $sender_info['full_name'] }}</b></a>
                                                            @if(!empty($dialog['garant']) && $dialog['garant']->id == $sender_info['id'])
                                                                <a style="color: #f43a59;">(гарант)</a>
                                                            @endif
                                                        @else
                                                            <a style="color: #0ad251;"><b>{{ $sender_info['full_name'] }}</b></a>
                                                            @if($message->destination_id != 0)
                                                                <span style="color: #9ba2aa;">для {{ App\User::find($message->destination_id)->getFullName() }}</span>
                                                            @endif
                                                        @endif
                                                        <br>
                                                        @if(isset($sender_info['show_html']) && $sender_info['show_html'] == true)
                                                            {!! nl2br($message->message) !!}
                                                        @else
                                                            {{ nl2br($message->message) }}
                                                        @endif
                                                        <span style="float: right;">{{ dateConvert($message->created_at->timestamp) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                    <div>
                        <form method="POST" action="{{ route('messages.send') }}">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" name="recipient" value="{{ $dialog['members']->id }}">
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="dialog" value="{{ $dialog_id }}">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" placeholder="Введите сообщение" required></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block" id="send-btn">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="howToModal" tabindex="-1" role="dialog" aria-labelledby="howToModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="howToModalTitle">Как проходит сделка</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                После нажатия кнопки <b>&laquo;Купить&raquo;</b> на странице с объявлением, Вы должны оплатить покупку.
                <br>
                На страницу с оплатой Вы можете перейти, нажав по соответствующей кнопке в сообщении, которое отправила Вам система.
                <br><br>
                После успешной оплаты автор объявления (далее &ndash; продавец) будет уведомлен о новой сделке.
                <br>
                Теперь вы с продавцом можете договорится о том, как Вам будут переданы вирты. Во время процесса, Вы можете также вызвать гаранта, если возникнут какие-то трудности.
                <br>
                Как только продавец подтвердит передачу виртов, Вы будете об этом уведомлены и также сможете подтверить получение. После этого сделка завершится.
                <br><br>
                <div class="alert alert-danger">
                    <b>Внимание!</b>
                    <br>
                    &ndash; Если Вы не оплатите товар в течении часа после начала сделки, сделка автоматически удаляется.
                    <br>
                    &ndash; Если в любом из последующих этапов (после оплаты) сделка неактивна более 24-х часов, она также автоматически удаляется.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(() => {
            $('#messagebox').scrollTop($('#messagebox')[0].scrollHeight);
        });
    </script>
@endsection
