@extends('app')

@section('body')
<div class="row mb-5">
    <div class="col-md-12" style="margin-bottom: 15px;">
        <a href="{{ route('messages') }}" class="btn btn-primary btn-block">Вернуться к сообщениям</a>
    </div>
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="col-md-12" >
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            </div>
        @endforeach
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Отправка сообщения
            </div>
            <div class="card-body">
                <form method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" id="recipient" name="recipient" placeholder="Введите ID получателя" value="{{ $user_id }}" required>
                        <div style="display: none;" class="form-control-feedback text-warning" id="hint">Loading...</div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Введите сообщение" required></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block" id="send-btn">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        function onRecipientChange(){
            var recipient_id = Number($('#recipient').val());
            
            $.ajax({
                method: 'GET',
                url: '/users/find/'+recipient_id,
                dataType: 'json',
                success: (data) => {
                    if(data.found){
                        $('#recipient').attr('class', 'form-control border border-success');
                        $('#hint').attr('class', 'form-control-feedback text-success');
                        $('#hint').html('Сообщение для <a href="'+data.user.url+'">'+data.user.full_name+'</a>');
                    
                        $('#send-btn').attr('type', 'submit');
                        $('#send-btn').removeClass('disabled');
                    } else {
                        $('#recipient').attr('class', 'form-control border border-danger');
                        $('#hint').attr('class', 'form-control-feedback text-danger');
                        $('#hint').html('Пользователь не найден.');

                        $('#send-btn').attr('type', 'button');
                        $('#send-btn').addClass('disabled');
                    }
                    $('#hint').attr('style', '');
                }
            });
        }

        $(document).ready(() => {
            @if(isset($user_id))
                onRecipientChange();
            @endif
        });

        $('#recipient').change(() => {
            onRecipientChange();
        });
    </script>
@endsection