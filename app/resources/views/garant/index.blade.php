@extends('app')

@section('body')
    @include('tpl.counters')
    <div class="row mb-5">
        <div class="col-md-12" style="margin-top: 30px;">
            <div class="card">
                <div class="card-header">
                    Сделки, которые нуждаются в гаранте
                </div>
                <div class="card-body">
                    @if(count($deals) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Покупатель</th>
                                    <th>Объявление</th>
                                    <th>Статус</th>
                                    <th>Дата начала</th>
                                    <th>Дата последнего изменения</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($deals as $deal)
                                    <tr>
                                        <td><b>{{ $deal->id }}</b></td>
                                        <td><b><a href="{{ route('users.view', ['user_id' => $deal->user_id])}}" target="_blank">{{ App\User::find($deal->user_id)->getFullName() }}</a></b></td>
                                        <td><b><a href="{{ route('adverts.view', ['advert_id' => $deal->advert_id])}}" target="_blank">Перейти</a></b></td>
                                        <td><b>{{ $deal->getStatus() }}</b></td>
                                        <td><b>{{ dateConvert($deal->created_at->timestamp) }}</b></td>
                                        <td><b>{{ dateConvert($deal->updated_at->timestamp) }}</b></td>
                                        <td><a href="{{ route('garant.accept', ['deal_id' => $deal->id]) }}" class="btn btn-success btn-sm">Согласится</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $deals->links() }}
                    @else
                        <div class="alert alert-danger">Ничего не найдено.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
