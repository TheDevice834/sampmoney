@extends('app')

@section('body')
@include('tpl.counters')
<div class="row mb-5">
    <div class="col-md-12">
        <div class="text-center">
            <button class="btn btn-dark">
                <h6 style="color: white;">У Вас на балансе</h6>
                <h1 style="margin-top: -20px;margin-bottom: -5px;color: white;">
                    {{ $user->balance }} руб.
                </h1>
            </button>
            <br>
            <a class="btn btn-success" style="margin-top: -15px;">Пополнить</a>
            <a class="btn btn-primary" style="margin-top: -15px;">Вывести</a>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 30px;">
        <div class="card">
            <div class="card-header">
                История расходов/пополнений
            </div>
            <div class="card-body">
                @if(count($costs) > 0)
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Тип</th>
                                <th>Описание</th>
                                <th>Сумма</th>
                                <th>Дата</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($costs as $cost)
                                <tr>
                                    <td><b>{{ $cost->id }}</b></td>
                                    <td><b>{{ $cost->showType() }}</b></td>
                                    <td><b>{{ $cost->text }}</b></td>
                                    <td><b>{{ $cost->amount }} руб.</b></td>
                                    <td><b>{{ dateConvert($cost->created_at->timestamp) }}</b></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $costs->links() }}
                @else
                    <div class="alert alert-info">Ничего не найдено.</div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection