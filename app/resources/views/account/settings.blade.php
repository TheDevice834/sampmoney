@extends('app')

@section('body')
@include('tpl.counters')
<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Настройки
            </div>
            <div class="card-body">
                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {!! \Session::get('success') !!}
                    </div>
                @endif
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                <form method="POST">
                    @csrf
                    <dl class="row">
                        <dt class="col-sm-4">Выводить ссылку на мой ВКонтакте в профиле</dt>
                        <dd class="col-sm-8">
                            <select name="vk_link" class="form-control">
                                <option value="1"{{ ($user->show_vk_link == 1) ? ' selected' : '' }}>Да</option>
                                <option value="0"{{ ($user->show_vk_link == 0) ? ' selected' : '' }}>Нет</option>
                            </select>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-4">Выводить мое имя</dt>
                        <dd class="col-sm-8">
                            <select name="show_name" class="form-control">
                                <option value="1"{{ ($user->show_name == 1) ? ' selected' : '' }}>Да</option>
                                <option value="0"{{ ($user->show_name == 0) ? ' selected' : '' }}>Нет</option>
                            </select>
                        </dd>
                    </dl>
                    <hr>
                    <button type="submit" class="btn btn-primary btn-gradient btn-block">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection