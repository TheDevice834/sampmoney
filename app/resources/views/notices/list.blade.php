@extends('app')

@section('body')
@include('tpl.counters')
<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Уведомления
                <p class="task-list-stats">
                    {{ $unread }} непрочитанных
                </p>
            </div>
            <div class="card-body">
                @if(count($notices) > 0)
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th></th>
                                <th>Статус</th>
                                <th>Заголовок</th>
                                <th>Содержимое</th>
                                <th>Время</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notices as $notice)
                                <tr>
                                    <td><a href="{{ route('notices.view', ['notice_id' => $notice->id]) }}"><b>{{ $notice->id }}</b></a></td>
                                    <td><i class="batch-icon batch-{{ $notice->getTypeInfo()['icon'] }}"></i></td>
                                    @if($notice->is_read == 1)
                                        <td><span class="badge badge-success">Прочитано</span></td>
                                    @else
                                        <td><span class="badge badge-danger">Не прочитано</span></td>
                                    @endif
                                    <td><b>{{ $notice->getTypeInfo()['title'] }}</b></td>
                                    <td><b>{{ $notice->short_text }}</b></td>
                                    <td><b>{{ dateConvert($notice->created_at->timestamp) }}</b></td>
                                </tr>
                                @php
                                    $notice->makeRead();
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                    {{ $notices->links() }}
                @else
                    <div class="alert alert-info">У Вас нет уведомлений.</div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection