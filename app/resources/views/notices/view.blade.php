@extends('app')

@section('body')
@include('tpl.counters')
<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                {{ $notice->getTypeInfo()['title'] }}
                <p class="task-list-stats">
                        Дата получения: {{ dateConvert($notice->created_at->timestamp) }}
                </p>
                <div class="header-btn-block" style="top: 31px;">
                    <a href="{{ url()->previous() }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                        Назад
                    </a>
                </div>
            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-3">ID уведомления</dt>
                    <dd class="col-sm-9">{{ $notice->id }}</dd>

                    <dt class="col-sm-3">Тип уведомления</dt>
                    <dd class="col-sm-9">{{ $notice->getTypeInfo()['title'] }}</dd>

                    <dt class="col-sm-3">Иконка уведомления</dt>
                    <dd class="col-sm-9"><i class="batch-icon batch-{{ $notice->getTypeInfo()['icon'] }}"></i></dd>
                    
                    <dt class="col-sm-3">Статус</dt>
                    <dd class="col-sm-9">{!! $notice->is_read == 1 ? '<span class="badge badge-green">Прочитано</span>' : '<span class="badge badge-danger">Не прочитано</span>' !!}</dd>

                    <dt class="col-sm-3">Получатель</dt>
                    <dd class="col-sm-9">{{ Auth::user()->getFullName() }}</dd>

                    <dt class="col-sm-3">Текст</dt>
                    <dd class="col-sm-9">{!! $notice->full_text !!}</dd>
                </dl>
            </div>
        </div>
    </div>
</div>
@php
$notice->makeRead();
@endphp
@endsection