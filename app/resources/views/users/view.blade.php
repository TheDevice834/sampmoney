@extends('app')

@section('body')
@include('tpl.counters')
        <div class="col-md-15">
            <div class="card">
                <div class="card-user-profile">
                    <div class="profile-page-left">
                        <div class="row">
                            <div class="col-lg-12 mb-4">
                                <div class="profile-picture profile-picture-lg bg-gradient bg-primary mb-4">
                                    <img src="{{ $user->avatar }}" width="144" height="144">
                                </div>
                                @if($auth_user->id != $user->id)
                                    <a class="btn btn-primary btn-block btn-gradient" href="{{ route('messages.send') }}?user={{ $user->id }}">
                                        <i class="batch-icon batch-icon-mail-incoming"></i>
                                        Сообщение
                                    </a>
                                    <a id="friend-request-btn" class="btn btn-primary btn-block btn-gradient">
                                        <i id="friend-request-icon" class="batch-icon batch-icon-user-alt-add-2"></i>
                                        <span id="friend-request-text">Loading...</span>
                                    </a>
                                @else
                                    <a class="btn btn-primary btn-block btn-gradient disabled">
                                        <i class="batch-icon batch-icon-mail-incoming"></i>
                                        Сообщение
                                    </a>
                                    <a class="btn btn-primary btn-block btn-gradient disabled">
                                        <i class="batch-icon batch-icon-user-alt-add-2"></i>
                                        Добавить в друзья
                                    </a>
                                @endif
                                @if($user->show_vk_link)
                                    <a class="btn btn-primary btn-block" href="https://vk.com/id{{ $user->vk_id }}" target="_blank">
                                        <i class="batch-icon batch-icon-user-card"></i>
                                        Профиль ВКонтакте
                                    </a>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <h5 class="my-0">Продаж</h5>
                                <div class="h3 my-0">
                                    <a href="#">0</a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h5 class="my-0">Покупок</h5>
                                <div class="h3 my-0">
                                    <a href="#">0</a>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <h5>
                            <i class="batch-icon batch-icon-users"></i>
                            Друзья
                        </h5>
                        <div class="profile-page-block-outer clearfix">
                            @if(count($friends) > 0)
                                @foreach($friends as $friend)
                                    <div class="profile-page-block">
                                        <a href="{{ route('users.view', ['user_id' => $friend->id]) }}">
                                            <div class="profile-picture bg-gradient bg-primary">
                                                <img src="{{ $friend->avatar }}" width="44" height="44">
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                                <a class="float-right mt-2" href="#">Ещё</a>
                            @else
                                Нет друзей
                            @endif
                        </div>
                    </div>
                    <div class="profile-page-center">
                        @if(\Session::has('success'))
                            <div class="alert alert-success">
                                {!! \Session::get('success') !!}
                            </div>
                        @endif
                        @if($errors->any())
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif
                        <h1 class="card-user-profile-name" style="margin-bottom: -10px;">
                            {{ $user->getFullName() }}
                        </h1>
                        <p>
                            {{ $user->getOnline() }}
                        </p>
                        @if($auth_user->id == $user->id)
                            <div class="comment-block">
                                <form method="POST" action="{{ route('wall.post', ['user_id' => $user->id]) }}">
                                    @csrf
                                    <div class="form-group">
                                        <textarea class="form-control" name="text" rows="2" placeholder="Введите текст для публикации..." required></textarea>
                                        <div class="media-feed-control clearfix">
                                            <button type="submit" class="btn btn-secondary btn-sm comment-reply float-right">Опубликовать</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                        <hr>
                        <ul class="list-unstyled mt-5">
                            @foreach($wall as $post)
                                <li class="media">
                                    <div class="profile-picture bg-gradient bg-primary mb-4">
                                        <img src="{{ $user->avatar }}" width="44" height="44">
                                    </div>
                                    <div class="media-body" id="post{{ $post->id }}">
                                        <div class="media-title mt-0 mb-1">
                                            <a href="{{ route('users.view', ['user_id' => $user->id]) }}">{{ $user->getFullName() }}</a> <small> {{ dateConvert($post->created_at->timestamp) }}</small>
                                        </div>
                                        {{ $post->text }}
                                        <div class="media-feed-control">
                                            <a id="post{{ $post->id }}-link" @if($post->isUserLike($auth_user->id)) style="color:black;" @endif onclick="likePost({{ $post->id }}, '{{ route('wall.like', ['post_id' => $post->id]) }}')">
                                                <i class="batch-icon batch-icon-heart-full"></i>
                                                <span id="post{{ $post->id }}-likes">Мне {{ ($post->isUserLike($auth_user->id)) ? 'не ' : '' }}нравится ({{ $post->getLikes() }})</span>
                                            </a>
                                            <a>
                                                <i class="batch-icon batch-icon-speech-bubble-left-tip"></i>
                                                Комментарии ({{ $post->getCommentsCount() }})
                                            </a>
                                            <a data-toggle="modal" data-target="#complaintModal" onclick="makeComplaint({{ $user->id }}, 1,{{ $post->id }})">
                                                <i class="batch-icon batch-icon-flag"></i>
                                                Жалоба
                                            </a>
                                            <a href="{{ route('wall.view', ['post_id' => $post->id]) }}">
                                                <i class="batch-icon batch-icon-document"></i>
                                                Перейти
                                            </a>
                                            @if($post->getLikes() > 0)
                                                <a style="float: right;" data-toggle="modal" data-target="#likesModal" onclick="whoLiked('{{ route('wall.get', ['post_id' => $post->id]) }}')">
                                                    <i class="batch-icon batch-icon-heart-full"></i>
                                                    Кто оценил?
                                                </a>
                                            @endif
                                        </div>
                                        <div class="media-body-reply-block">
                                        <ul class="list-unstyled">
                                            @if($post->getCommentsCount() > 0)
                                                @foreach($post->getComments() as $comment)
                                                    @php
                                                        $commentAuthor = $comment->getAuthor();
                                                    @endphp
                                                    <li class="media mt-4">
                                                        <div class="profile-picture bg-gradient bg-primary mb-4">
                                                            <img src="{{ $commentAuthor->avatar }}" width="44" height="44">
                                                        </div>
                                                        <div class="media-body">
                                                            <div class="media-title mt-0 mb-1">
                                                                <a href="{{ route('users.view', ['user_id' => $commentAuthor->id]) }}">{{ $commentAuthor->getFullName() }}</a> <small> {{ dateConvert($comment->created_at->timestamp) }}</small>
                                                            </div>
                                                            {{ $comment->text }}
                                                            <div class="media-feed-control">
                                                                <a id="comment{{ $comment->id }}-link" @if($comment->isUserLike($auth_user->id)) style="color:black;" @endif onclick="likeComment({{ $comment->id }}, '{{ route('comment.like', ['comment_id' => $comment->id]) }}')">
                                                                    <i class="batch-icon batch-icon-heart-full"></i>
                                                                    <span id="comment{{ $comment->id }}-likes">Мне {{ ($comment->isUserLike($auth_user->id)) ? 'не ' : '' }}нравится ({{ $comment->getLikes() }})</span>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#complaintModal" onclick="makeComplaint({{ $commentAuthor->id }}, 2,{{ $comment->id }})">
                                                                    <i class="batch-icon batch-icon-flag"></i>
                                                                    Жалоба
                                                                </a>
                                                                @if($comment->getLikes() > 0)
                                                                    <a style="float: right;" data-toggle="modal" data-target="#likesModal" onclick="whoCommentLiked('{{ route('comment.get', ['comment_id' => $comment->id]) }}')">
                                                                        <i class="batch-icon batch-icon-heart-full"></i>
                                                                        Кто оценил?
                                                                    </a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                            <li class="comment-reply-block mt-4">
                                                <form action="{{ route('comment.post', ['post_id' => $post->id]) }}" method="POST" class="comment-form">
                                                    @csrf
                                                    <div class="form-group">
                                                        <textarea class="form-control comment-reply-textarea" name="text" rows="2" placeholder="Введите текст Вашего комментария..." required></textarea>
                                                        <button type="submit" class="btn btn-secondary btn-sm comment-reply float-right">Ответить</button>
                                                    </div>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="likesModal" tabindex="-1" role="dialog" aria-labelledby="likesModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="likesModalLabel">Loading...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="complaintModal" tabindex="-1" role="dialog" aria-labelledby="complaintModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="complaintModalLabel">Жалоба</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="sendComplaint" action="{{ route('users.complaint') }}" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            @csrf
                            <input type="hidden" id="culprit_id" name="culprit_id">
                            <input type="hidden" id="object" name="object">
                            <input type="hidden" id="object_id" name="object_id">
                            <span>Причина:</span>
                            <select class="form-control" name="cause">
                                @foreach(App\Complaint::getCauses() as $key => $cause)
                                    <option value="{{ $key }}">{{ $cause }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Отправить</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
        <script>
            function whoLiked(url){
                $('#likesModal').on('show.bs.modal', function(event){
                    var modal = $(this);

                    $.ajax({
                        method: 'GET',
                        url: url,
                        dataType: 'json',
                        success: (data) => {
                            modal.find('.modal-title').text(data.count+' пользователям понравилась запись');
                            modal.find('.row').html('');

                            data.likes.forEach((like) => {
                                modal.find('.row').append('<div class="col-md-2"><a href="'+like.user.profile_url+'" title="'+like.user.full_name+'"><img src="'+like.user.avatar+'" class="rounded" width="100%"></a></div>');
                            });
                        }
                    });
                });
            }
            function whoCommentLiked(url){
                $('#likesModal').on('show.bs.modal', function(event){
                    var modal = $(this);

                    $.ajax({
                        method: 'GET',
                        url: url,
                        dataType: 'json',
                        success: (data) => {
                            modal.find('.modal-title').text(data.count+' пользователям понравился комментарий');
                            modal.find('.row').html('');

                            data.likes.forEach((like) => {
                                modal.find('.row').append('<div class="col-md-2"><a href="'+like.user.profile_url+'" title="'+like.user.full_name+'"><img src="'+like.user.avatar+'" class="rounded" width="100%"></a></div>');
                            });
                        }
                    });
                });
            }
            function likePost(post_id, url){
                $.ajax({
                    method: 'GET',
                    url: url,
                    dataType: 'json',
                    success: (data) => {
                        updateLikes(post_id, data.likes, data.is_liked);
                    }
                });
            }
            function likeComment(comment_id, url){
                $.ajax({
                    method: 'GET',
                    url: url,
                    dataType: 'json',
                    success: (data) => {
                        updateCommentLikes(comment_id, data.likes, data.is_liked);
                    }
                });
            }
            function updateLikes(post_id, likes, is_liked){
                if(is_liked){
                    $('#post'+post_id+'-likes').text('Мне не нравится ('+likes+')');
                    $('#post'+post_id+'-link').attr('style', 'color:black;');
                } else {
                    $('#post'+post_id+'-likes').text('Мне нравится ('+likes+')');
                    $('#post'+post_id+'-link').attr('style', '');
                }
            }

            function updateCommentLikes(comment_id, likes, is_liked){
                if(is_liked){
                    $('#comment'+comment_id+'-likes').text('Мне не нравится ('+likes+')');
                    $('#comment'+comment_id+'-link').attr('style', 'color:black;');
                } else {
                    $('#comment'+comment_id+'-likes').text('Мне нравится ('+likes+')');
                    $('#comment'+comment_id+'-link').attr('style', '');
                }
            }

            function makeComplaint(culprit_id, object, object_id){
                $('#complaintModal').on('show.bs.modal', function(event){
                    var modal = $(this);

                    $('#culprit_id').val(culprit_id);
                    $('#object').val(object);
                    $('#object_id').val(object_id);
                });
            }

            $("#sendComplaint").on("submit", (e) => {
                var values = {};
                $.each($('#sendComplaint').serializeArray(), function(i, field) {
                    values[field.name] = field.value;
                });

                $.ajax({
                    method: 'POST',
                    url: '{{ route('users.complaint') }}',
                    data: values,
                    dataType: 'json',
                    success: (data) => {
                        if(data.success == true){
                            toastr.success(data.message, 'Жалоба');
                        } else {
                            toastr.error(data.message, 'Жалоба');
                        }
                    }
                });

                $('#complaintModal').modal('toggle');

                return false;
            });

            @if($auth_user->id != $user->id)
                var currentState = {{ $friend_status }};

                var states = [
                    {
                        icon: 'batch-icon batch-icon-user-alt-add-2',
                        text: 'Добавить в друзья',
                        link: '{{ route('users.friends.add', ['user_id' => $user->id]) }}'
                    }, {
                        icon: 'batch-icon batch-icon-user-alt-remove-2',
                        text: 'Отписаться',
                        link: '{{ route('users.friends.remove', ['user_id' => $user->id]) }}'
                    }, {
                        icon: 'batch-icon batch-icon-user-alt-remove-2',
                        text: 'Удалить из друзей',
                        link: '{{ route('users.friends.remove', ['user_id' => $user->id]) }}'
                    }
                ];

                function updateState(state){
                    $('#friend-request-text').text(states[state].text);
                    $('#friend-request-icon').attr('class', states[state].icon);
                }
            @endif

            $(document).ready(() => {
                @if($auth_user->id != $user->id)
                    updateState(currentState);

                    $('#friend-request-btn').on('click', () => {
                        if(currentState == 0){
                            var url = states[0].link;
                        } else {
                            var url = states[2].link;
                        }

                        console.log(url);

                        $.ajax({
                            method: 'GET',
                            url: url,
                            dataType: 'json',
                            success: (data) => {
                                currentState = data.state;
                                updateState(currentState);
                            }
                        });
                    });
                @endif
            });
        </script>
@endsection
