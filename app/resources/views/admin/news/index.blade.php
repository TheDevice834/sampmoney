@extends('admin')

@section('body')
    <div class="row">
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
        @endif
        @if(\Session::has('success'))
            <div class="col-md-12" >
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{ $title }}
                    <a href="{{ route('admin.news.add') }}" class="btn btn-success" style="float:right;margin-top: -10px;"><b>Добавить <i class="mdi mdi-plus"></i></b></a>
                    <hr>
                    @if(count($news) > 0)
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Заголовок</th>
                                    <th>Видимость</th>
                                    <th>Автор</th>
                                    <th>Дата<br>публикации</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $post)
                                    <tr>
                                        <td>{{ $post->id }}</td>
                                        <td><small><a href="{{ route('news.view', ['post_id' => $post->id]) }}" target="_blank"><b>{{ str_limit($post->title, 20, '...') }}</b></a></small></td>
                                        <td><small>{{ str_limit($post->getAccessLevelTitle(), 30, '...') }}</small></td>
                                        <td><small><a href="{{ route('users.view', ['user_id' => $post['author']->id]) }}" target="_blank"><b>{{ $post['author']->getFullName() }}</b></a></small></td>
                                        <td><small>{{ dateConvert($post->created_at->timestamp) }}</small></td>
                                        <td>
                                            <small>
                                                <a href="{{ route('admin.news.edit', ['post_id' => $post->id]) }}"><b>Редактировать</b></a> /
                                                <a href="{{ route('admin.news.delete', ['post_id' => $post->id]) }}"><b>Удалить</b></a>
                                            </small>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $news->links() }}
                    @else
                        <div class="alert alert-danger">Новостей не найдено.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection