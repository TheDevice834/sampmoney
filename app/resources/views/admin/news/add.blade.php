@extends('admin')

@section('body')
    <div class="row">
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
        @endif
        @if(\Session::has('success'))
            <div class="col-md-12" >
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $title }}</h4>
                    <form class="forms-sample" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Заголовок</label>
                            <div class="col-sm-9">
                                <input type="text" name="title" class="form-control" placeholder="Заголовок новости" value="{{ old('title') }}{{ (isset($post)) ? $post->title : '' }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Видимость</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="access_level" required>
                                    <option value="0" @if(isset($post) && $post->access_level == 0) selected @endif>Видна всем</option>
                                    <option value="1" @if(isset($post) && $post->access_level == 1) selected @endif>Видна только авторизированным пользователям</option>
                                    <option value="2" @if(isset($post) && $post->access_level == 2) selected @endif>Видна только гостям</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                                Текст новости
                                <br>
                                <span class="text-muted">(можно использовать HTML)</span>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="text" class="form-control" rows="10" placeholder="Текст новости" required>{{ old('text') }}{{ (isset($post)) ? $post->text : '' }}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success mr-2">
                            @if(isset($post))
                                Сохранить
                            @else
                                Добавить
                            @endif
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection