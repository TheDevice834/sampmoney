@extends('admin')

@section('body')
    <div class="row">
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
        @endif
        @if(\Session::has('success'))
            <div class="col-md-12" >
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{ $title }}
                    <hr>
                    @if(count($adverts) > 0)
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Автор</th>
                                    <th>Сервер</th>
                                    <th>Сумма</th>
                                    <th>Цена</th>
                                    <th>Метод передачи</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($adverts as $advert)
                                    <tr>
                                        <td>{{ $advert->id }}</td>
                                        <td><small><a href="{{ route('users.view', ['user_id' => $advert['author']->id]) }}" target="_blank"><b>{{ $advert['author']->getFullName() }}</b></a></small></td>
                                        <td><small>{{ $advert->getServerName() }}</small></td>
                                        <td><small>${{ moneyConvert($advert->amount) }}</small></td>
                                        <td><small>{{ moneyConvert($advert->price) }} руб.</small></td>
                                        <td><small>{{ App\Advert::getTransferMethods()[$advert->transfer_method] }}</small></td>
                                        <td>
                                            @if($advert->is_hide == 1)
                                                <small class="text-muted">Удалить</small>
                                            @else
                                                <a href="{{ route('admin.adverts.delete', ['advert_id' => $advert->id]) }}"><small><b>Удалить</b></small></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $adverts->links() }}
                    @else
                        <div class="alert alert-danger">Объявлений не найдено.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection