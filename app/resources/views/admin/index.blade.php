@extends('admin')

@php
$deals = App\Deal::where('status', 4)->get();

$turnover = 0;
// $created_at = 0;
// $updated_at = 0;

if(count($deals) > 0){
    foreach($deals as $deal){
        if($advert = App\Advert::find($deal->advert_id)){
            $turnover += $advert->amount;
        }

        // $created_at += $deal->created_at->timestamp;
        // $updated_at += $deal->updated_at->timestamp;
    }

    // $created_at_average = $created_at / count($deals);
    // $updated_at_average = $updated_at / count($deals);

    // $average = $updated_at_average - $created_at_average;
} // else $average = 0;

$counters = [
    'users' => App\User::count(),
    'turnover' => $turnover,
    'adverts' => App\Advert::count(),
    'deals' => count($deals)
];
@endphp

@section('body')
<div class="row">
		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
		<div class="card card-statistics">
			<div class="card-body">
				<div class="clearfix">
					<div class="float-left">
						<i class="mdi mdi-cube text-danger icon-lg"></i>
					</div>
					<div class="float-right">
						<p class="mb-0 text-right">Общий оборот</p>
						<div class="fluid-container">
							<h3 class="font-weight-medium text-right mb-0">${{ moneyConvert($counters['turnover']) }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
		<div class="card card-statistics">
			<div class="card-body">
				<div class="clearfix">
					<div class="float-left">
						<i class="mdi mdi-receipt text-warning icon-lg"></i>
					</div>
					<div class="float-right">
						<p class="mb-0 text-right">Сделок</p>
						<div class="fluid-container">
							<h3 class="font-weight-medium text-right mb-0">{{ $counters['deals'] }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
		<div class="card card-statistics">
			<div class="card-body">
				<div class="clearfix">
					<div class="float-left">
						<i class="mdi mdi-account-location text-success icon-lg"></i>
					</div>
					<div class="float-right">
						<p class="mb-0 text-right">Пользователей</p>
						<div class="fluid-container">
							<h3 class="font-weight-medium text-right mb-0">{{ $counters['users'] }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
		<div class="card card-statistics">
			<div class="card-body">
				<div class="clearfix">
					<div class="float-left">
						<i class="mdi mdi-poll-box text-info icon-lg"></i>
					</div>
					<div class="float-right">
						<p class="mb-0 text-right">Объявлений</p>
						<div class="fluid-container">
							<h3 class="font-weight-medium text-right mb-0">{{ $counters['adverts'] }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection