@extends('admin')

@section('body')
    <div class="row">
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
        @endif
        @if(\Session::has('success'))
            <div class="col-md-12" >
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{ $title }}
                    <hr>
                    @if(count($requests) > 0)
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Сделка</th>
                                    <th>Гарант</th>
                                    <th>В пользу</th>
                                    <th>Дата</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td>{{ $request->id }}</td>
                                        <td><small><a href="{{ route('deal.redirect', ['deal_id' => $request->deal_id]) }}" target="_blank"><b>#{{ $request->deal_id }}</b></a></small></td>
                                        <td><small><a href="{{ route('users.view', ['user_id' => $request->garant_id]) }}" target="_blank"><b>{{ App\User::find($request->garant_id)->getFullName() }}</b></a></small></td>
                                        <td>
                                            <small>
                                                @if($request->in_favor == 1)
                                                    Продавца
                                                @else
                                                    Покупателя
                                                @endif
                                            </small>
                                        </td>
                                        <td><small>{{ dateConvert($request->timestamp) }}</small></td>
                                        <td>
                                            <small>
                                                <b>
                                                    @if($request->status == 1)
                                                        Одобрена
                                                    @elseif($request->status == 2)
                                                        Отклонена
                                                    @else
                                                        <a href="{{ route('admin.cancelreq.reject', ['req_id' => $request->id]) }}">Отклонить</a> /
                                                        <a href="{{ route('admin.cancelreq.approve', ['req_id' => $request->id]) }}">Одобрить</a>
                                                    @endif
                                                </b>
                                            </small>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $requests->links() }}
                    @else
                        <div class="alert alert-danger">Заявок не найдено.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection