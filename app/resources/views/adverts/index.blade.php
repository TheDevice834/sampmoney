@extends('app')

@section('body')
    @include('tpl.counters')
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ $title }}
                    <p class="task-list-stats">
                        Всего {{ $adverts_count }} объявлений
                    </p>
                    <div class="header-btn-block" style="top: 31px;">
                        <a href="{{ route('adverts.add') }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                            Продать или купить вирты
                        </a>
                    </div>
                </div>
                @if($filter_on)
                    <div class="card-body">
                        Применен фильтр {!! $filter['name'] !!}.
                        <br>
                        По Вашему запросу найдено <b>{{ $filter['found'] }}</b> объявлений.
                    </div>
                @endif
                <div class="card-table">
                    @if($adverts_count > 0)

                        <table class="table table-hover align-middle">
                            <thead class="thead-default">
                            <tr>
                                <th>Пользователь</th>
                                <th>Тип</th>
                                <th>Проект</th>
                                <th>Сумма</th>
                                <th class="text-center">Рейтинг продавца</th>
                                <th>Публикация (МСК)</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($adverts as $advert)
                                <tr>
                                    <td>
                                        <div class="media">
                                            <div class="profile-picture bg-gradient bg-primary has-message float-right d-flex mr-3">
                                                <img src="{{ $advert->getUser()->avatar }}" width="44" height="44">
                                            </div>
                                            <div class="media-body">
                                                <div class="heading mt-1">
                                                    <a href="{{ route('users.view', ['user_id' => $advert->getUser()->id]) }}">{{ $advert->getUser()->getFullName() }}</a>
                                                </div>
                                                <div class="subtext">{{ $advert->getUser()->getOnline() }}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="{{ route('adverts') }}?type={{ $advert->type }}" data-toggle="tooltip" data-placement="top" title="Фильтр по типу">{{ $advert->getTypes()[$advert->type] }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('adverts') }}?server={{ $advert->getServerName() }}" data-toggle="tooltip" data-placement="top" title="Фильтр по серверу">{{ $advert->getServerName() }}</a>
                                    </td>
                                    <td>
                                        <b>${{ moneyConvert($advert->amount) }}</b>
                                        <br>
                                        за <b>{{ moneyConvert($advert->price) }}</b> руб.
                                    </td>
                                    <td class="text-center">
                                        <span class="badge badge-primary">Объявлений: {{ count($advert->getUser()->getAdverts()) }}</span>
                                    </td>
                                    <td>{{ dateConvert($advert->created_at->timestamp) }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary btn-gradient" href="{{ route('adverts.view', ['advert_id' => $advert->id]) }}" data-toggle="tooltip" data-placement="top" title="Перейти">
                                            <i class="batch-icon batch-icon-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $adverts->links() }}
                        
                    @else
                        <div class="card-body">
                            <div class="alert alert-danger">Объявления не найдены.</div>
                        </div>
                    @endif
                    @if($filter_on)
                        <a href="{{ route('adverts') }}" class="btn btn-primary btn-block" style="border-radius: 0px;">
                            Все объявления
                            <br>
                            <small>Выше показаны объявления с применением фильтра</small>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

    </script>
@endsection