@extends('app')

@section('body')
@include('tpl.counters')
<div class="row mb-5">
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="col-md-12" >
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            </div>
        @endforeach
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                {{ $title }}
                <p class="task-list-stats">
                    Объявление #{{ $advert->id }}
                </p>
                <div class="header-btn-block" style="top: 31px;">
                    @if($author->id == $auth->id)
                        <div class="btn-group" style="margin-top: 2px;">
                            <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Управление
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop2">
                                <a class="dropdown-item" href="{{ route('adverts.delete', ['advert_id' => $advert->id]) }}">Удалить</a>
                            </div>
                        </div>
                    @endif
                    <a href="{{ url()->previous() }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                        Назад
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3">
                        <img src="{{ $author->avatar }}" width="44" height="44" style="float: left;margin-right: 15px;border-radius: 50%;">
                        <div style="margin-top: 3px;">
                            <a href="{{ route('users.view', ['user_id' => $author->id]) }}" data-toggle="tooltip" data-placement="top" title="Автор объявления">{{ str_limit($author->getFullName(), 15, '...') }}</a>
                            <br>
                            {{ $author->getOnline() }}
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div style="margin-top: 3px;">
                            Сумма: <b>${{ moneyConvert($advert->amount) }}</b>
                            <br>
                            Стоимость: <b>{{ moneyConvert($advert->price) }} руб.</b>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div style="margin-top: 3px;">
                            Сервер: <b>{{ $server }}</b>
                            <br>
                            Дата публикации: <b>{{ dateConvert($advert->created_at->timestamp) }}</b>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    @if($author->id == $auth->id)
                        <div class="col-lg-3">
                            <a class="btn btn-danger btn-block disabled">Жалоба</a>
                        </div>
                        <div class="col-lg-2">
                            <a class="btn btn-success btn-block disabled">Купить</a>
                        </div>
                    @else
                        <div class="col-lg-3">
                            <a class="btn btn-danger btn-block" data-toggle="modal" data-target="#complaintModal" onclick="makeComplaint()">Жалоба</a>
                        </div>
                        <div class="col-lg-2">
                            <a href="{{ route('adverts.deal.start', ['advert_id' => $advert->id]) }}" class="btn btn-success btn-block">Купить</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="complaintModal" tabindex="-1" role="dialog" aria-labelledby="complaintModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="complaintModalLabel">Жалоба</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="sendComplaint" action="{{ route('users.complaint') }}" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        @csrf
                        <input type="hidden" name="culprit_id" value="{{ $author->id }}">
                        <input type="hidden" name="object" value="3">
                        <input type="hidden" name="object_id" value="{{ $advert->id }}">
                        <span>Причина:</span>
                        <select class="form-control" name="cause">
                            @foreach(App\Complaint::getCauses() as $key => $cause)
                                <option value="{{ $key }}">{{ $cause }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Отправить</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        function makeComplaint(){
            $('#complaintModal').on('show.bs.modal', function(event){
                var modal = $(this);
            });
        }

        $("#sendComplaint").on("submit", (e) => {
            var values = {};
            $.each($('#sendComplaint').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });

            $.ajax({
                method: 'POST',
                url: '{{ route('users.complaint') }}',
                data: values,
                dataType: 'json',
                success: (data) => {
                    if(data.success == true){
                        toastr.success(data.message, 'Жалоба');
                    } else {
                        toastr.error(data.message, 'Жалоба');
                    }
                }
            });

            $('#complaintModal').modal('toggle');

            return false;
        });
    </script>
@endsection
