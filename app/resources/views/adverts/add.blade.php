@extends('app')

@section('body')
<div class="row mb-5">
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="col-md-12" >
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            </div>
        @endforeach
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                {{ $title }}
            </div>
            <div class="card-body">
                <form method="POST">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-12">
                            <select name="type" class="form-control" style="margin-bottom: 5px;" required>
                                <option value="0" selected>Выберите тип объявления</option>
                                @foreach($types as $key => $type)
                                    <option value="{{ $key }}">{{ $type }} виртов</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <select name="server" id="project" class="form-control" style="margin-bottom: 5px;" required>
                                <option value="0" selected>Выберите проект</option>
                                @foreach($servers as $server)
                                    <option value="{{ $server->id }}">{{ $server->name }}</option>
                                @endforeach
                                <option value="another">Другой</option>
                            </select>
                            <div style="display: none;" class="form-control-feedback" id="project_ip">Loading...</div>
                        </div>
                    </div>
                    <div class="form-group row" style="display: none;" id="project_field">
                        <div class="col-md-12">
                            <input type="hidden" class="form-control" id="project_namej" name="project_name" placeholder="Введите название проекта" required>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="number" class="form-control" name="amount" placeholder="Введите количество виртов" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <select name="transfer_method" class="form-control" required>
                                <option value="0" selected>Способ передачи\получения виртов</option>
                                @foreach($transfer_methods as $key => $method)
                                    <option value="{{ $key }}">{{ $method }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="number" class="form-control" name="price" placeholder="Стоимость продажи\покупки (руб.)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="g-recaptcha" data-sitekey="{{ env('RECAPTCHA_CLIENT') }}"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var projects_ip = {
            @foreach($servers as $server)
                '{{ $server->id }}': '{{ $server->ip }}',
            @endforeach
        };
        function showAnotherProjectField(){
            $('#project_field').attr('style', '');
            $('#project_namej').attr('type', 'text');
            $('#project_ip').attr('style', 'display: none;');
        }

        function hideAnotherProjectField(){
            $('#project_field').attr('style', 'display: none;');
            $('#project_namej').attr('type', 'hidden');
        }

        $('#project').on('change', (e) => {
            var val = $('#project').val();

            if(val == 'another') showAnotherProjectField();
            else {
                hideAnotherProjectField();

                if(val != '0'){
                    $('#project_ip').attr('style', '');
                    $('#project_ip').text('IP: '+projects_ip[val]);
                } else $('#project_ip').attr('style', 'display: none;');
            }
        });
    </script>
@endsection