@extends('app')

@php
if(Auth::check()){
	$isAuth = true;
	$user = Auth::user();
} else {
	$isAuth = false;
}
@endphp

@section('body')
@include('tpl.counters')
@if(!$isAuth)
    <style>
        .modal {
            text-align: center;
        }

        @media screen and (min-width: 768px) {
            .modal:before {
                display: inline-block;
                vertical-align: middle;
                content: " ";
                height: 100%;
            }
        }

        .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }
    </style>
    <div class="modal fade" id="authModal" tabindex="-1" role="dialog" aria-labelledby="authModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="authModalTitle">Авторизация</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a href="{{ route('auth') }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                        Войти с помощью ВКонтакте
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body text-center">
                    <button type="button" class="btn btn-primary btn-gradient waves-effect waves-light" data-toggle="modal" data-target="#authModal">
                        Авторизироваться
                    </button>
                    <p>
                        Чтобы начать пользоваться всем функционалом сайта, Вам нужно авторизироваться.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row mb-5">
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="col-md-12">
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            </div>
        @endforeach
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Список объявлений
                <p class="task-list-stats">
                    Всего {{ $adverts_count }} объявлений
                </p>
                <div class="header-btn-block" style="top: 31px;">
                    <a href="{{ route('adverts.add') }}" class="btn btn-secondary btn-gradient waves-effect waves-light">
                        Продать или купить вирты
                    </a>
                </div>
            </div>
            <div class="card-table">
                @if($adverts_count > 0)
                    <table class="table table-hover align-middle">
                        <thead class="thead-default">
                            <tr>
                                <th>Пользователь</th>
                                <th>Тип</th>
                                <th>Проект</th>
                                <th>Сумма</th>
                                <th class="text-center">Рейтинг продавца</th>
                                <th>Публикация (МСК)</th>
                                <th class="text-right">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($adverts as $advert)
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="profile-picture bg-gradient bg-primary has-message float-right d-flex mr-3">
                                            <img src="{{ $advert->getUser()->avatar }}" width="44" height="44">
                                        </div>
                                        <div class="media-body">
                                            <div class="heading mt-1">
                                                <a href="{{ route('users.view', ['user_id' => $advert->getUser()->id]) }}">{{ $advert->getUser()->getFullName() }}</a>
                                            </div>
                                            <div class="subtext">{{ $advert->getUser()->getOnline() }}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('adverts') }}?type={{ $advert->type }}" data-toggle="tooltip" data-placement="top" title="Фильтр по типу">{{ $advert->getTypes()[$advert->type] }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('adverts') }}?server={{ $advert->getServerName() }}" data-toggle="tooltip" data-placement="top" title="Фильтр по серверу">{{ $advert->getServerName() }}</a>
                                </td>
                                <td>
                                    <b>${{ moneyConvert($advert->amount) }}</b>
                                    <br>
                                    за <b>{{ moneyConvert($advert->price) }}</b> руб.
                                </td>
                                <td class="text-center">
                                    <span class="badge badge-primary">Объявлений: {{ count($advert->getUser()->getAdverts()) }}</span>
                                </td>
                                <td>{{ dateConvert($advert->created_at->timestamp) }}</td>
                                <td class="text-right">
                                    <a class="btn btn-primary btn-gradient" href="{{ route('adverts.view', ['advert_id' => $advert->id]) }}" data-toggle="tooltip" data-placement="top" title="Перейти">
                                        <i class="batch-icon batch-icon-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <a href="{{ route('adverts') }}" class="btn btn-primary btn-block" style="border-radius: 0px;">
                        Все объявления
                        <br>
                        <small>Выше показаны последние 10 объявлений</small>
                    </a>
                @else
                    <div class="card-body">
                        <div class="alert alert-danger">Объявления не найдены.</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row mb-5">
    <div class="col-md-8">
        <div class="card card-activity card-md">
            <div class="card-header">
                Последняя активность
            </div>
			@if(count($activity) > 0)
            	<div class="card-media-list">
					@foreach($activity as $item)
		                <div class="media clickable" data-qp-link="{{ route('users.view', ['user_id' => $item['user']->id]) }}">
		                    <div class="profile-picture bg-gradient bg-primary has-message float-right d-flex mr-3">
		                        <img src="{{ $item['user']->avatar }}" width="44" height="44">
		                    </div>
		                    <div class="media-body">
		                        <div class="heading mt-1">
		                            {!! $item['activity']->text !!}
									<span style="float: right;">{{ dateConvert($item['activity']->created_at->timestamp) }}</span>
		                        </div>
		                        <div class="subtext">{{ $item['user']->getFullName() }}</div>
		                    </div>
		                </div>
					@endforeach
            	</div>
			@else
				<div class="card-body">
					<div class="alert alert-info">
						Ничего не найдено.
					</div>
				</div>
			@endif
        </div>
    </div>
    <div class="col-md-4 col-md">
        <div class="card card-md card-team-members">
            <div class="card-header">
                Гаранты онлайн
            </div>
            <div class="card-media-list">
                @if(count($garants) > 0)
                    @foreach($garants as $garant)
                        <div class="media clickable" data-qp-link="{{ route('users.view', ['user_id' => $garant->id]) }}">
                            <div class="profile-picture bg-gradient bg-primary has-message float-right d-flex mr-3">
                                <img src="{{ $garant->avatar }}" width="44" height="44">
                            </div>
                            <div class="media-body">
                                <div class="heading mt-1">
                                    {{ $garant->getFullName() }}
                                </div>
                                <div class="subtext">{{ $garant->getOnline() }}</div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="media clickable">
                        <div class="media-body">
                            <div class="alert alert-danger">Нет гарантов онлайн.</div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(window).on('load',function(){
            $('#authModal').modal('show');
        });
    </script>
@endsection
