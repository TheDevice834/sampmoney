@php
$user = Auth::user();

$notices_count = App\Notice::where('user_id', $user->id)->where('is_read', 0)->count();
$notices = $user->getNotices();
@endphp
<!DOCTYPE html>
<html>
<head>
	@include('tpl.meta')
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" href="{{ route('home') }}/assets/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/admin/vendors/css/vendor.bundle.base.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/admin/vendors/css/vendor.bundle.addons.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/admin/css/style.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div class="container-scroller">
	<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
		<div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
			<a class="navbar-brand brand-logo" href="{{ route('admin') }}">
				<img src="{{ route('home') }}/assets/admin/images/logo.svg" alt="logo" />
			</a>
			<a class="navbar-brand brand-logo-mini" href="{{ route('admin') }}">
				<img src="{{ route('home') }}/assets/admin/images/logo-mini.svg" alt="logo" />
			</a>
		</div>
		<div class="navbar-menu-wrapper d-flex align-items-center">
			<ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
				<li class="nav-item">
					<a href="{{ route('admin') }}" class="nav-link">
					<i class="mdi mdi-comment-alert-outline"></i>Жалобы</a>
				</li>
			</ul>
			<ul class="navbar-nav navbar-nav-right">
				<li class="nav-item dropdown">
					<a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
						<i class="mdi mdi-bell"></i>
						@if($notices_count > 0) <span class="count">{{ $notices_count }}</span> @endif
					</a>
					<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
						@if($notices_count > 0)
							<div class="dropdown-item">
								<p class="mb-0 font-weight-normal float-left">{{ $notices_count }} новых уведомления
								</p>
								<a href="{{ route('notices') }}" class="badge badge-warning badge-pill float-right" style="color: white;">Посмотреть все</a>
							</div>
							<div class="dropdown-divider"></div>
							@foreach($notices as $notice)
								<a href="{{ route('notices.view', ['notice_id' => $notice->id]) }}" class="dropdown-item preview-item">
									<div class="preview-thumbnail">
										<div class="preview-icon bg-success">
											<i class="mdi mdi-alert-circle-outline mx-0"></i>
										</div>
									</div>
									<div class="preview-item-content">
										<h6 class="preview-subject font-weight-medium text-dark">{{ $notice->getTypeInfo()['title'] }}</h6>
										<p class="font-weight-light small-text">
											{{ dateConvert($notice->created_at->timestamp) }}
										</p>
									</div>
								</a>
								<div class="dropdown-divider"></div>
							@endforeach
						@else
							<div class="dropdown-item">
								<p class="mb-0 font-weight-normal float-left">Нет новых уведомлений
								</p>
								<a href="{{ route('notices') }}" class="badge badge-warning badge-pill float-right" style="color: white;">Посмотреть все</a>
							</div>
						@endif
					</div>
				</li>
				<li class="nav-item dropdown d-none d-xl-inline-block">
					<a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
						<span class="profile-text">{{ $user->getFullName() }}</span>
						<img class="img-xs rounded-circle" src="{{ $user->avatar }}" alt="Profile image">
					</a>
					<div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
						<a class="dropdown-item p-0">
							<div class="d-flex border-bottom">
								<a href="{{ route('admin.adverts') }}" class="py-3 px-4 d-flex align-items-center justify-content-center">
									<i class="mdi mdi-domain mr-0 text-gray"></i>
								</a>
								<a href="{{ route('admin') }}" class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
									<i class="mdi mdi-account-outline mr-0 text-gray"></i>
								</a>
								<a href="{{ route('admin') }}" class="py-3 px-4 d-flex align-items-center justify-content-center">
									<i class="mdi mdi-alarm-check mr-0 text-gray"></i>
								</a>
							</div>
						</a>
						<a href="{{ route('admin') }}" class="dropdown-item mt-2">
							Пользователи
						</a>
						<a href="{{ route('messages') }}" class="dropdown-item">
							Сообщения
						</a>
						<a href="{{ route('logout') }}" class="dropdown-item">
							Выйти
						</a>
					</div>
				</li>
			</ul>
			<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
				<span class="mdi mdi-menu"></span>
			</button>
		</div>
	</nav>
	<div class="container-fluid page-body-wrapper">
		<nav class="sidebar sidebar-offcanvas" id="sidebar">
			<ul class="nav">
				<li class="nav-item nav-profile">
					<div class="nav-link">
						<div class="user-wrapper">
							<div class="profile-image">
								<img src="{{ $user->avatar }}" alt="profile image">
							</div>
							<div class="text-wrapper">
								<p class="profile-name">{{ $user->getFullName() }}</p>
								<div>
									<small class="designation text-muted">Администратор</small>
									<span class="status-indicator online"></span>
								</div>
							</div>
						</div>
						<a href="{{ route('admin.news.add') }}" class="btn btn-success btn-block"><b>Добавить новость</b>
							<i class="mdi mdi-plus"></i>
						</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('home') }}">
						<i class="menu-icon mdi mdi-arrow-left"></i>
						<span class="menu-title">Обратно на сайт</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin') }}">
						<i class="menu-icon mdi mdi-television"></i>
						<span class="menu-title">Панель управления</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.news') }}">
						<i class="menu-icon mdi mdi-newspaper"></i>
						<span class="menu-title">Новости</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.adverts') }}">
						<i class="menu-icon mdi mdi-domain"></i>
						<span class="menu-title">Объявления</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.cancelreq') }}">
						<i class="menu-icon mdi mdi-content-paste"></i>
						<span class="menu-title">Заявки на отмену сделки</span>
					</a>
				</li>
			</ul>
		</nav>
		<div class="main-panel">
			<div class="content-wrapper">
				@yield('body')
			</div>
			<footer class="footer">
				<div class="container-fluid clearfix">
					<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
					<a href="{{ route('home') }}" target="_blank">{{ env('APP_NAME') }}</a>. Все права защищены.</span>
					<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
						<i class="mdi mdi-heart text-danger"></i>
					</span>
				</div>
			</footer>
		</div>
	</div>
</div>

	<script src="{{ route('home') }}/assets/admin/vendors/js/vendor.bundle.base.js"></script>
	<script src="{{ route('home') }}/assets/admin/vendors/js/vendor.bundle.addons.js"></script>
	<script src="{{ route('home') }}/assets/admin/js/off-canvas.js"></script>
	<script src="{{ route('home') }}/assets/admin/js/misc.js"></script>
	@yield('scripts')
</body>
</html>