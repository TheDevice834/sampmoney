<div class="row mb-5">
    <div class="col-md-12">
        <footer>
            <center>
	            2018 © {{ env('APP_NAME') }} - все права защищены.
	            <br>
	            <b>{{ getEngineVersion() }}</b>
	        </center>
        </footer>
    </div>
</div>