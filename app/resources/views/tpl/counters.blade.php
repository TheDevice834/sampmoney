@php
$deals = App\Deal::where('status', 4)->get();

$turnover = 0;
$created_at = 0;
$updated_at = 0;

if(count($deals) > 0){
    foreach($deals as $deal){
        if($advert = App\Advert::find($deal->advert_id)){
            $turnover += $advert->amount;
        }

        $created_at += $deal->created_at->timestamp;
        $updated_at += $deal->updated_at->timestamp;
    }

    $created_at_average = $created_at / count($deals);
    $updated_at_average = $updated_at / count($deals);

    $average = $updated_at_average - $created_at_average;
} else $average = 0;

$counters = [
    'users' => App\User::count(),
    'turnover' => $turnover,
    'adverts' => App\Advert::count(),
    'average' => $average
];
@endphp
<div class="row">
    <div class="col-md-6 col-lg-6 col-xl-3 mb-5">
        <div class="card card-tile card-xs bg-primary bg-gradient text-center">
            <div class="card-body p-4">
                <div class="tile-left">
                    <i class="batch-icon batch-icon-user-alt batch-icon-xxl"></i>
                </div>
                <div class="tile-right">
                    <div class="tile-number">{{ $counters['users'] }}</div>
                    <div class="tile-description">Пользователей</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3 mb-5">
        <div class="card card-tile card-xs bg-secondary bg-gradient text-center">
            <div class="card-body p-4">
                <div class="tile-left">
                    <i class="batch-icon batch-icon-tag-alt-2 batch-icon-xxl"></i>
                </div>
                <div class="tile-right">
                    <div class="tile-number">${{ moneyConvert($counters['turnover']) }}</div>
                    <div class="tile-description">Оборот за все время</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3 mb-5">
        <div class="card card-tile card-xs bg-primary bg-gradient text-center">
            <div class="card-body p-4">
                <div class="tile-left">
                    <i class="batch-icon batch-icon-list batch-icon-xxl"></i>
                </div>
                <div class="tile-right">
                    <div class="tile-number">{{ $counters['adverts'] }}</div>
                    <div class="tile-description">Действующих объявлений</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3 mb-5">
        <div class="card card-tile card-xs bg-secondary bg-gradient text-center">
            <div class="card-body p-4">
                <div class="tile-left">
                    <i class="batch-icon batch-icon-star batch-icon-xxl"></i>
                </div>
                <div class="tile-right">
                    <div class="tile-number">{{ timeConvert($counters['average']) }}</div>
                    <div class="tile-description">минут среднее время сделки</div>
                </div>
            </div>
        </div>
    </div>
</div>