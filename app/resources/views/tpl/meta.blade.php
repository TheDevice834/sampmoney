@php
if(isset($title)) $title = $title;
else $title = env('APP_NAME').' - Продать или купить игровую валюту SAMP онлайн';
@endphp
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="{{ route('home') }}/assets/img/favicon.png">
<title>{{ $title }} / {{ env('APP_NAME') }}</title>