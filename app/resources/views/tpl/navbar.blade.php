@php
if(Auth::check()){
	$isAuth = true;
	$user = Auth::user();

	$unread_messages = $user->getUnreadMessages();
	$unread_messages_count = count($unread_messages);

	$notices_count = App\Notice::where('user_id', $user->id)->where('is_read', 0)->count();
	$notices = $user->getNotices();
	$news_query = App\News::where('access_level', '<', 2)->orderBy('id', 'DESC')->limit(4);

	if($unread_messages_count > 0){
		$notices_count++;
	}
} else {
	$isAuth = false;
	$news_query = App\News::where('access_level', '<>', 1)->orderBy('id', 'DESC')->limit(4);
}
$news = $news_query->get();
$news_count = $news_query->count();
@endphp
<nav class="navbar navbar-expand-lg navbar-light bg-white">
	<a class="navbar-brand" href="{{ route('home') }}">
		<img src="{{ route('home') }}/assets/img/logo-dark.png" width="145" height="32.3" alt="QuillPro">
	</a>


	<div class="navbar-collapse" id="navbar-header-content">
		<ul class="navbar-nav navbar-language-translation mr-auto">
			<li class="nav-item dropdown">
				<li class="nav-item{{ Route::current()->getName() === 'home' ? ' active' : '' }}">
					<a class="nav-link" href="{{ route('home') }}">Главная</a>
				</li>
				<li class="nav-item{{ Route::current()->getName() === 'news' ? ' active' : '' }}">
					<a class="nav-link" href="{{ route('news') }}">Новости</a>
				</li>
			</li>
		</ul>
		<ul class="navbar-nav navbar-notifications float-right">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle no-waves-effect" id="navbar-notification-misc" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
					<i class="batch-icon batch-icon-newspaper-alt"></i>
					@if($news_count > 0)
						<span class="notification-number">{{ $news_count }}</span>
					@endif
				</a>
				<ul class="dropdown-menu dropdown-menu-right dropdown-menu-md" aria-labelledby="navbar-notification-misc">
					@if($news_count > 0)
						@foreach($news as $post)
							<li class="media">
								<a href="{{ route('news.view', ['post_id' => $post->id]) }}">
									<div class="media-body">
										<h6 class="mt-0 mb-1 notification-heading">{{ $post->title }}</h6>
										<span class="notification-time">{{ dateConvert($post->created_at->timestamp) }}</span>
									</div>
								</a>
							</li>
						@endforeach
						<li class="media">
							<a href="{{ route('news') }}">
								<div class="media-body">
									<div class="notification-text text-center">
										<b>Посмотреть все новости</b>
									</div>
								</div>
							</a>
						</li>
					@else
						<li class="media">
							<a>
								<div class="media-body">
									<div class="notification-text text-center">
										Нет новостей
									</div>
								</div>
							</a>
						</li>
					@endif
				</ul>
			</li>
			@if($isAuth)
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle no-waves-effect" id="navbar-notification-calendar" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
						<i class="batch-icon batch-icon-bell"></i>
						@if($notices_count > 0)
							<span class="notification-number">{{ $notices_count }}</span>
						@endif
					</a>
					<ul class="dropdown-menu dropdown-menu-right dropdown-menu-md" aria-labelledby="navbar-notification-calendar">
						@if($unread_messages_count > 0)
							<li class="media">
								<a href="{{ route('messages') }}">
									<i class="batch-icon batch-icon-mail-incoming batch-icon-xl d-flex mr-3"></i>
									<div class="media-body">
										<h6 class="mt-0 mb-1 notification-heading">Новые сообщения</h6>
										<div class="notification-text">
											У Вас {{ $unread_messages_count }} непрочитанных сообщений.
										</div>
									</div>
								</a>
							</li>
						@endif
						@if($notices_count > 0)
							@foreach($notices as $notice)
								<li class="media">
									<a href="{{ route('notices.view', ['notice_id' => $notice->id]) }}">
										<i class="batch-icon batch-{{ $notice->getTypeInfo()['icon'] }} batch-icon-xl d-flex mr-3"></i>
										<div class="media-body">
											<h6 class="mt-0 mb-1 notification-heading">{{ $notice->getTypeInfo()['title'] }}</h6>
											<div class="notification-text">
												{{ $notice->short_text }}
											</div>
											<span class="notification-time">{{ dateConvert($notice->created_at->timestamp) }}</span>
										</div>
									</a>
								</li>
							@endforeach
						@else
							<li class="media">
								<a>
									<div class="media-body">
										<div class="notification-text text-center">
											Нет непрочитанных уведомлений
										</div>
									</div>
								</a>
							</li>
						@endif
						<li class="media">
							<a href="{{ route('notices') }}">
								<div class="media-body">
									<div class="notification-text text-center">
										<b>Посмотреть все уведомления</b>
									</div>
								</div>
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<ul class="navbar-nav ml-5 navbar-profile">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" id="navbar-dropdown-navbar-profile" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
						<div class="profile-name">
							{{ $user->getFullName() }}
						</div>
						<div class="profile-picture bg-gradient bg-primary has-message float-right">
							<img src="{{ $user->avatar }}" width="44" height="44">
						</div>
					</a>
					<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-dropdown-navbar-profile">
						<li><a class="dropdown-item" href="{{ route('users.view', ['user_id' => $user->id]) }}">Профиль</a></li>
						<li><a class="dropdown-item" href="{{ route('balance') }}">
							Баланс
							<span class="badge badge-primary badge-pill float-right">{{ $user->balance }} руб.</span>
						</a></li>
						<li>
							<a class="dropdown-item" href="{{ route('messages') }}">
								Сообщения
								<span class="badge badge-danger badge-pill float-right">{{ $unread_messages_count }}</span>
							</a>
						</li>
                        @if($user->isAdmin())
						    <li><a class="dropdown-item" href="{{ route('admin') }}">Панель управления</a></li>
						@endif
						@if($user->isGarant())
						    <li><a class="dropdown-item" href="{{ route('garant') }}">Панель гаранта</a></li>
						@endif
                        <li><a class="dropdown-item" href="{{ route('settings') }}">Настройки</a></li>
						<li><a class="dropdown-item" href="{{ route('logout') }}">Выход</a></li>
					</ul>
				</li>
			</ul>
		@endif
	</div>
</nav>
