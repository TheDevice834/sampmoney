@extends('app')

@section('body')
    @include('tpl.counters')
    <div class="row mb-5">
        @if(\Session::has('success'))
            <div class="col-md-12">
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
            </div>
        @endif
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
        @endif
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Стена
                    <p class="task-list-stats">
                        Публикация <b>#{{ $post->id }}</b>
                    </p>
                    <div class="header-btn-block" style="top: 31px;">
                        <a href="{{ url()->previous() }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                            Назад
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <img src="{{ $author->avatar }}" width="44" height="44" style="float: left;margin-right: 15px;border-radius: 50%;">
                            <div style="margin-top: 3px;">
                                <a href="{{ route('users.view', ['user_id' => $author->id]) }}" data-toggle="tooltip" data-placement="top" title="Автор публикации">{{ str_limit($author->getFullName(), 15, '...') }}</a>
                                <br>
                                {{ $author->getOnline() }}
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div style="margin-top: 3px;">
                                Количество лайков: <b>{{ $post->getLikes() }}</b>
                                <br>
                                Количество комментариев: <b>{{ $post->getCommentsCount() }}</b>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div style="margin-top: 3px; float:right;">
                                <br>
                                Дата публикации: <b>{{ dateConvert($post->created_at->timestamp) }}</b>
                            </div>
                        </div>
                        <div class="col-lg-12" style="margin-top: 15px;font-size: 15px;">
                            {{ $post->text  }}
                        </div>
                        <br><br><br>
                        <div class="col-lg-3">
                            <a id="post{{ $post->id }}-link" class="btn btn-primary btn-block" @if($post->isUserLike($auth_user->id)) style="color:black;" @endif onclick="likePost({{ $post->id }}, '{{ route('wall.like', ['post_id' => $post->id]) }}')">
                                <span id="post{{ $post->id }}-likes">Мне {{ ($post->isUserLike($auth_user->id)) ? 'не ' : '' }}нравится ({{ $post->getLikes() }})</span>
                            </a>
                        </div>
                        <div class="col-lg-2">
                            <a class="btn btn-outline-danger btn-block" data-toggle="modal" data-target="#complaintModal" onclick="makeComplaint({{ $author->id }}, 1, {{ $post->id }})">
                                Жалоба
                            </a>
                        </div>
                        <div class="col-lg-2">
                            @if($author->id == $auth_user->id)
                                <div class="btn-group">
                                    <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Управление
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop2">
                                        <a class="dropdown-item" href="{{ route('wall.edit', ['post_id' => $post->id]) }}">Редактировать</a>
                                        <a class="dropdown-item" href="{{ route('wall.delete', ['post_id' => $post->id]) }}">Удалить</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @if($post->getLikes() > 0)
                            <div class="col-lg-5">
                                <a style="float: right;margin-top:0px;" class="btn btn-secondary" data-toggle="modal" data-target="#likesModal" onclick="whoLiked('{{ route('wall.get', ['post_id' => $post->id]) }}')">
                                    Кто оценил?
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@if($post->getCommentsCount() > 0)
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Комментарии к публикации
                    <p class="task-list-stats">
                        Всего <b>{{ $post->getCommentsCount() }}</b> комментариев
                    </p>
                </div>
                <div class="card-body">
                    @foreach($post->getComments() as $comment)
                        @php
                            $commentAuthor = $comment->getAuthor();
                        @endphp
                        <div class="col-lg-12" id="comment{{ $comment->id }}" style="margin-bottom: 15px;border: 1px double #e0e0e0;padding: 10px;border-radius:5px;">
                            <div class="row">
                                <div class="col-lg-3">
                                    <img src="{{ $commentAuthor->avatar }}" width="44" height="44" style="float: left;margin-right: 15px;border-radius: 50%;">
                                    <div style="margin-top: 3px;">
                                        <a href="{{ route('users.view', ['user_id' => $commentAuthor->id]) }}" data-toggle="tooltip" data-placement="top" title="Автор комментария">{{ str_limit($commentAuthor->getFullName(), 15, '...') }}</a>
                                        <br>
                                        {{ $commentAuthor->getOnline() }}
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div style="margin-top: 3px;">
                                        {{ dateConvert($comment->created_at->timestamp) }} написал комментарий:
                                        <br>
                                        <div style="font-weight: 500;">{{ $comment->text }}</div>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="padding-left: 10px;">
                                    <a style="float: right;border-radius: 0px;" id="comment{{ $comment->id }}-link" class="btn btn-primary btn-sm" @if($comment->isUserLike($auth_user->id)) style="color:black;" @endif onclick="likeComment({{ $comment->id }}, '{{ route('comment.like', ['comment_id' => $comment->id]) }}')">
                                        <span id="comment{{ $comment->id }}-likes">Мне {{ ($comment->isUserLike($auth_user->id)) ? 'не ' : '' }}нравится ({{ $comment->getLikes() }})</span>
                                    </a>
                                    <a style="float: right;border-radius: 0px;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#complaintModal" onclick="makeComplaint({{ $commentAuthor->id }}, 2, {{ $comment->id }})">
                                        Жалоба
                                    </a>
                                    @if($commentAuthor->id == $auth_user->id)
                                        <div class="btn-group" style="float: right;margin-top: 6px;">
                                            <button type="button" class="btn btn-dark btn-sm dropdown-toggle" style="border-radius: 0px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Управление
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{ route('comment.edit', ['comment_id' => $comment->id]) }}">Редактировать</a>
                                                <a class="dropdown-item" href="{{ route('comment.delete', ['comment_id' => $comment->id]) }}">Удалить</a>
                                            </div>
                                        </div>
                                    @endif
                                    @if($comment->getLikes() > 0)
                                        <a style="float: right;border-radius: 0px;" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#likesModal" onclick="whoCommentLiked('{{ route('comment.get', ['comment_id' => $comment->id]) }}')">
                                            Кто оценил?
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
    <div class="modal fade" id="likesModal" tabindex="-1" role="dialog" aria-labelledby="likesModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="likesModalLabel">Loading...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="complaintModal" tabindex="-1" role="dialog" aria-labelledby="complaintModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="complaintModalLabel">Жалоба</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="sendComplaint" action="{{ route('users.complaint') }}" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            @csrf
                            <input type="hidden" id="culprit_id" name="culprit_id">
                            <input type="hidden" id="object" name="object">
                            <input type="hidden" id="object_id" name="object_id">
                            <span>Причина:</span>
                            <select class="form-control" name="cause">
                                @foreach(App\Complaint::getCauses() as $key => $cause)
                                    <option value="{{ $key }}">{{ $cause }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Отправить</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function whoLiked(url){
            $('#likesModal').on('show.bs.modal', function(event){
                var modal = $(this);

                $.ajax({
                    method: 'GET',
                    url: url,
                    dataType: 'json',
                    success: (data) => {
                        modal.find('.modal-title').text(data.count+' пользователям понравилась запись');
                        modal.find('.row').html('');

                        data.likes.forEach((like) => {
                            modal.find('.row').append('<div class="col-md-2"><a href="'+like.user.profile_url+'" title="'+like.user.full_name+'"><img src="'+like.user.avatar+'" class="rounded" width="100%"></a></div>');
                        });
                    }
                });
            });
        }
        function whoCommentLiked(url){
            $('#likesModal').on('show.bs.modal', function(event){
                var modal = $(this);

                $.ajax({
                    method: 'GET',
                    url: url,
                    dataType: 'json',
                    success: (data) => {
                        modal.find('.modal-title').text(data.count+' пользователям понравился комментарий');
                        modal.find('.row').html('');

                        data.likes.forEach((like) => {
                            modal.find('.row').append('<div class="col-md-2"><a href="'+like.user.profile_url+'" title="'+like.user.full_name+'"><img src="'+like.user.avatar+'" class="rounded" width="100%"></a></div>');
                        });
                    }
                });
            });
        }
        function likePost(post_id, url){
            $.ajax({
                method: 'GET',
                url: url,
                dataType: 'json',
                success: (data) => {
                    updateLikes(post_id, data.likes, data.is_liked);
                }
            });
        }
        function likeComment(comment_id, url){
            $.ajax({
                method: 'GET',
                url: url,
                dataType: 'json',
                success: (data) => {
                    updateCommentLikes(comment_id, data.likes, data.is_liked);
                }
            });
        }
        function updateLikes(post_id, likes, is_liked){
            if(is_liked){
                $('#post'+post_id+'-likes').text('Мне не нравится ('+likes+')');
                $('#post'+post_id+'-link').attr('style', 'color:black;');
            } else {
                $('#post'+post_id+'-likes').text('Мне нравится ('+likes+')');
                $('#post'+post_id+'-link').attr('style', '');
            }
        }
        function updateCommentLikes(comment_id, likes, is_liked){
            if(is_liked){
                $('#comment'+comment_id+'-likes').text('Мне не нравится ('+likes+')');
                $('#comment'+comment_id+'-link').attr('style', 'color:black;float:right;');
            } else {
                $('#comment'+comment_id+'-likes').text('Мне нравится ('+likes+')');
                $('#comment'+comment_id+'-link').attr('style', 'float:right;');
            }
        }

        function makeComplaint(culprit_id, object, object_id){
            $('#complaintModal').on('show.bs.modal', function(event){
                var modal = $(this);

                $('#culprit_id').val(culprit_id);
                $('#object').val(object);
                $('#object_id').val(object_id);
            });
        }

        $("#sendComplaint").on("submit", (e) => {
            var values = {};
            $.each($('#sendComplaint').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });

            $.ajax({
                method: 'POST',
                url: '{{ route('users.complaint') }}',
                data: values,
                dataType: 'json',
                success: (data) => {
                    if(data.success == true){
                        toastr.success(data.message, 'Жалоба');
                    } else {
                        toastr.error(data.message, 'Жалоба');
                    }
                }
            });

            $('#complaintModal').modal('toggle');

            return false;
        });

        $(document).ready(() => {

        });
    </script>
@endsection
