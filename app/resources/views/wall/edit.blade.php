@extends('app')

@section('body')
<div class="row mb-5">
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="col-md-12" >
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            </div>
        @endforeach
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                {{ $title }}
            </div>
            <div class="card-body">
                <form method="POST">
                    @csrf
                    <div class="form-group row">
						<label class="col-sm-2 col-form-label">ID публикации</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="{{ $post->id }}" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Текст публикации</label>
						<div class="col-sm-10">
                            <textarea name="text" class="form-control" placeholder="Введите текст" required>{{ $post->text }}</textarea>
                        </div>
					</div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        
    </script>
@endsection