<!DOCTYPE html>
<html>
<head>
	@include('tpl.meta')
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" href="{{ route('home') }}/assets/fonts/batch-icons/css/batch-icons.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/css/bootstrap/mdb.min.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/css/hamburgers/hamburgers.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/plugins/toastr/toastr.min.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/fonts/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ route('home') }}/assets/css/quillpro/quillpro.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<style>
		.navbar .dropdown-menu a {
			font-weight: 500;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="right-column">
				@include('tpl.navbar')
				<main class="main-content p-5" role="main">
					@yield('body')
					@include('tpl.footer')
				</main>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{ route('home') }}/assets/js/jquery/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/js/bootstrap/popper.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/js/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/js/bootstrap/mdb.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/plugins/velocity/velocity.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/plugins/velocity/velocity.ui.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/plugins/jquery_visible/jquery.visible.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/js/misc/ie10-viewport-bug-workaround.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/js/misc/holder.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/plugins/toastr/toastr.min.js"></script>
	<script type="text/javascript" src="{{ route('home') }}/assets/js/scripts.js"></script>
	@yield('scripts')
</body>
</html>
