@extends('app')

@section('body')
@include('tpl.counters')
@if(!Auth::check())
    <style>
        .modal {
            text-align: center;
        }

        @media screen and (min-width: 768px) { 
            .modal:before {
                display: inline-block;
                vertical-align: middle;
                content: " ";
                height: 100%;
            }
        }

        .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }
    </style>
    <div class="modal fade" id="authModal" tabindex="-1" role="dialog" aria-labelledby="authModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="authModalTitle">Авторизация</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a href="{{ route('auth') }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                        Войти с помощью ВКонтакте
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body text-center">
                    <button type="button" class="btn btn-primary btn-gradient waves-effect waves-light" data-toggle="modal" data-target="#authModal">
                        Авторизироваться
                    </button>
                    <p>
                        Чтобы начать пользоваться всем функционалом сайта, Вам нужно авторизироваться.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                {!! $news->title !!}
                <p class="task-list-stats">
                    Дата публикации: {{ dateConvert($news->created_at->timestamp) }}
                </p>
                <div class="header-btn-block" style="top: 31px;">
                    <a href="{{ url()->previous() }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                        Назад
                    </a>
                </div>
            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-3">ID публикации</dt>
                    <dd class="col-sm-9">{{ $news->id }}</dd>

                    <dt class="col-sm-3">Автор публикации</dt>
                    <dd class="col-sm-9"><a href="{{ route('users.view', ['user_id' => $news->author_id]) }}">{{ $author->getFullName() }}</a></dd>
                </dl>
                {!! nl2br($news->text) !!}
            </div>
            <div class="card-footer">
                Прочитало данную публикацию: {{ $news->read_count }}
            </div>
        </div>
    </div>
</div>
@php
$news->makeRead();
@endphp
@endsection

@section('scripts')
    <script>
        $(window).on('load',function(){
            $('#authModal').modal('show');
        });
    </script>
@endsection