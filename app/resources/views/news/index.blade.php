@extends('app')

@section('body')
@include('tpl.counters')
@if(!Auth::check())
    <style>
        .modal {
            text-align: center;
        }

        @media screen and (min-width: 768px) { 
            .modal:before {
                display: inline-block;
                vertical-align: middle;
                content: " ";
                height: 100%;
            }
        }

        .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }
    </style>
    <div class="modal fade" id="authModal" tabindex="-1" role="dialog" aria-labelledby="authModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="authModalTitle">Авторизация</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a href="{{ route('auth') }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                        Войти с помощью ВКонтакте
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body text-center">
                    <button type="button" class="btn btn-primary btn-gradient waves-effect waves-light" data-toggle="modal" data-target="#authModal">
                        Авторизироваться
                    </button>
                    <p>
                        Чтобы начать пользоваться всем функционалом сайта, Вам нужно авторизироваться.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-md-12">
        <h1>Новости</h1>
    </div>
</div>
<div class="row mb-5">
    @if(count($news) > 0)
        @foreach($news as $post)
            <div class="col-md-12" style="margin-bottom: 20px;">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('news.view', ['post_id' => $post->id]) }}">{!! $post->title !!}</a>
                        <p class="task-list-stats">
                                ID публикации: {{ $post->id }}
                        </p>
                        <div class="header-btn-block" style="top: 31px;">
                            <a href="{{ route('news.view', ['post_id' => $post->id]) }}" class="btn btn-primary btn-gradient waves-effect waves-light">
                                Подробнее
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! nl2br($post->text) !!}
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Дата публикации: {{ dateConvert($post->created_at->timestamp) }}</li>
                        <li class="list-group-item">Автор публикации: <a href="{{ route('users.view', ['user_id' => $post->author_id]) }}">{{ ($author = App\User::find($post->author_id)) ? $author->getFullName() : 'No Name' }}</a></li>
                        <li class="list-group-item">Просмотров публикации: {{ $post->read_count }}</li>
                        <li class="list-group-item"></li>
                    </ul>
                </div>
            </div>
        @endforeach
        <div class="col-md-12">
            {{ $news->links() }}
        </div>
    @else
        <div class="col-md-12">
            <div class="alert alert-info">Материалы для показа не найдены.</div>
        </div>
    @endif
</div>
@endsection

@section('scripts')
    <script>
        $(window).on('load',function(){
            $('#authModal').modal('show');
        });
    </script>
@endsection