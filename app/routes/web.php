<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function(){
    return redirect()->route('home');
});
Route::get('/login', ['as' => 'login', 'uses' => function(){
    return redirect()->route('auth');
}]);

// MainController
Route::get('/', ['as' => 'home', 'uses' => 'MainController@index']);

// UsersController
Route::get('/@id{user_id}', ['as' => 'users.view', 'uses' => 'UsersController@view', 'middleware' => 'auth'])->where('user_id', '[0-9]+');
Route::get('/@id{user_id}/add-friend', ['as' => 'users.friends.add', 'uses' => 'UsersController@addFriend', 'middleware' => 'auth'])->where('user_id', '[0-9]+');
Route::get('/@id{user_id}/remove-friend', ['as' => 'users.friends.remove', 'uses' => 'UsersController@removeFriend', 'middleware' => 'auth'])->where('user_id', '[0-9]+');
Route::get('/users/find/{user_id}', ['as' => 'users.find', 'uses' => 'UsersController@find', 'middleware' => 'auth'])->where('user_id', '[0-9]+');
Route::post('/users/make-complaint', ['as' => 'users.complaint', 'uses' => 'UsersController@makeComplaint', 'middleware' => 'auth']);

// WallController
Route::get('/wall/{post_id}', ['as' => 'wall.view', 'uses' => 'WallController@view', 'middleware' => 'auth'])->where('post_id', '[0-9]+');
Route::get('/wall/get/{post_id}', ['as' => 'wall.get', 'uses' => 'WallController@get', 'middleware' => 'auth'])->where('post_id', '[0-9]+');
Route::get('/comment/get/{comment_id}', ['as' => 'comment.get', 'uses' => 'WallController@getComment', 'middleware' => 'auth'])->where('comment_id', '[0-9]+');
Route::post('/@id{user_id}/wall-post', ['as' => 'wall.post', 'uses' => 'WallController@post', 'middleware' => 'auth'])->where('user_id', '[0-9]+');
Route::post('/wall/{post_id}/comment-post', ['as' => 'comment.post', 'uses' => 'WallController@postComment', 'middleware' => 'auth'])->where('post_id', '[0-9]+');
Route::get('/wall/{post_id}/like', ['as' => 'wall.like', 'uses' => 'WallController@like', 'middleware' => 'auth'])->where('post_id', '[0-9]+');
Route::get('/comment/{comment_id}/like', ['as' => 'comment.like', 'uses' => 'WallController@commentLike', 'middleware' => 'auth'])->where('comment_id', '[0-9]+');
Route::get('/comment/edit/{comment_id}', ['as' => 'comment.edit', 'uses' => 'WallController@commentEdit', 'middleware' => 'auth'])->where('comment_id', '[0-9]+');
Route::post('/comment/edit/{comment_id}', ['as' => 'comment.edit', 'uses' => 'WallController@commentEdit', 'middleware' => 'auth'])->where('comment_id', '[0-9]+');
Route::get('/comment/delete/{comment_id}', ['as' => 'comment.delete', 'uses' => 'WallController@commentDelete', 'middleware' => 'auth'])->where('comment_id', '[0-9]+');
Route::get('/wall/edit/{post_id}', ['as' => 'wall.edit', 'uses' => 'WallController@edit', 'middleware' => 'auth'])->where('post_id', '[0-9]+');
Route::post('/wall/edit/{post_id}', ['as' => 'wall.edit', 'uses' => 'WallController@edit', 'middleware' => 'auth'])->where('post_id', '[0-9]+');
Route::get('/wall/delete/{post_id}', ['as' => 'wall.delete', 'uses' => 'WallController@delete', 'middleware' => 'auth'])->where('post_id', '[0-9]+');

// DealController
Route::get('/deal/redirect/{deal_id}', ['as' => 'deal.redirect', 'uses' => 'DealController@redirectToDealDialog', 'middleware' => 'auth'])->where('deal_id', '[0-9]+');
Route::get('/deal/pay/{deal_id}', ['as' => 'deal.pay', 'uses' => 'DealController@pay', 'middleware' => 'auth'])->where('deal_id', '[0-9]+');
Route::get('/deal/garant/{deal_id}', ['as' => 'deal.garant', 'uses' => 'DealController@garant', 'middleware' => 'auth'])->where('deal_id', '[0-9]+');
Route::get('/deal/transfer/{deal_id}', ['as' => 'deal.transfer', 'uses' => 'DealController@transfer', 'middleware' => 'auth'])->where('deal_id', '[0-9]+');
Route::get('/deal/cancel/{deal_id}', ['as' => 'deal.cancel', 'uses' => 'DealController@cancel', 'middleware' => 'auth'])->where('deal_id', '[0-9]+');

// MessagesController
Route::get('/messages', ['as' => 'messages', 'uses' => 'MessagesController@messages', 'middleware' => 'auth']);
Route::get('/messages/send', ['as' => 'messages.send', 'uses' => 'MessagesController@send', 'middleware' => 'auth']);
Route::post('/messages/send', ['as' => 'messages.send', 'uses' => 'MessagesController@send', 'middleware' => 'auth']);
Route::get('/messages/dialog/{dialog_id}', ['as' => 'messages.dialog', 'uses' => 'MessagesController@dialog', 'middleware' => 'auth'])->where('dialog_id', '[0-9]+');

// AdvertsController
Route::get('/adverts', ['as' => 'adverts', 'uses' => 'AdvertsController@index', 'middleware' => 'auth']);
Route::get('/adverts/{advert_id}', ['as' => 'adverts.view', 'uses' => 'AdvertsController@view', 'middleware' => 'auth'])->where('advert_id', '[0-9]+');
Route::get('/add', ['as' => 'adverts.add', 'uses' => 'AdvertsController@add', 'middleware' => 'auth']);
Route::post('/add', ['as' => 'adverts.add', 'uses' => 'AdvertsController@add', 'middleware' => 'auth']);
Route::get('/adverts/delete/{advert_id}', ['as' => 'adverts.delete', 'uses' => 'AdvertsController@delete', 'middleware' => 'auth'])->where('advert_id', '[0-9]+');
Route::get('/adverts/deal/start/{advert_id}', ['as' => 'adverts.deal.start', 'uses' => 'AdvertsController@startDeal', 'middleware' => 'auth'])->where('advert_id', '[0-9]+');

// AdminController
Route::get('/admin', ['as' => 'admin', 'uses' => 'AdminController@index', 'middleware' => 'admin']);

// Admin/NewsController
Route::get('/admin/news', ['as' => 'admin.news', 'uses' => 'Admin\NewsController@index', 'middleware' => 'admin']);
Route::get('/admin/news/add', ['as' => 'admin.news.add', 'uses' => 'Admin\NewsController@add', 'middleware' => 'admin']);
Route::post('/admin/news/add', ['as' => 'admin.news.add', 'uses' => 'Admin\NewsController@add', 'middleware' => 'admin']);
Route::get('/admin/news/{post_id}/edit', ['as' => 'admin.news.edit', 'uses' => 'Admin\NewsController@edit', 'middleware' => 'admin'])->where('post_id', '[0-9]+');
Route::post('/admin/news/{post_id}/edit', ['as' => 'admin.news.edit', 'uses' => 'Admin\NewsController@edit', 'middleware' => 'admin'])->where('post_id', '[0-9]+');
Route::get('/admin/news/{post_id}/delete', ['as' => 'admin.news.delete', 'uses' => 'Admin\NewsController@delete', 'middleware' => 'admin'])->where('post_id', '[0-9]+');

// Admin/AdvertsController
Route::get('/admin/adverts', ['as' => 'admin.adverts', 'uses' => 'Admin\AdvertsController@index', 'middleware' => 'admin']);
Route::get('/admin/adverts/{advert_id}/delete', ['as' => 'admin.adverts.delete', 'uses' => 'Admin\AdvertsController@delete', 'middleware' => 'admin'])->where('advert_id', '[0-9]+');

// Admin/CancelRequestsController
Route::get('/admin/cancelreq', ['as' => 'admin.cancelreq', 'uses' => 'Admin\CancelRequestsController@index', 'middleware' => 'admin']);
Route::get('/admin/cancelreq/{req_id}/reject', ['as' => 'admin.cancelreq.reject', 'uses' => 'Admin\CancelRequestsController@reject', 'middleware' => 'admin'])->where('req_id', '[0-9]+');
Route::get('/admin/cancelreq/{req_id}/approve', ['as' => 'admin.cancelreq.approve', 'uses' => 'Admin\CancelRequestsController@approve', 'middleware' => 'admin'])->where('req_id', '[0-9]+');

// GarantController
Route::get('/garant', ['as' => 'garant', 'uses' => 'GarantController@index', 'middleware' => 'garant']);
Route::get('/garant/call/{deal_id}', ['as' => 'garant.call', 'uses' => 'GarantController@call', 'middleware' => 'auth'])->where('deal_id', '[0-9]+');
Route::get('/garant/accept/{deal_id}', ['as' => 'garant.accept', 'uses' => 'GarantController@accept', 'middleware' => 'garant'])->where('deal_id', '[0-9]+');
Route::get('/garant/cancel/{deal_id}', ['as' => 'garant.cancel', 'uses' => 'GarantController@cancel', 'middleware' => 'garant'])->where('deal_id', '[0-9]+');


// AccountController
Route::get('/settings', ['as' => 'settings', 'uses' => 'AccountController@settings', 'middleware' => 'auth']);
Route::post('/settings', ['as' => 'settings', 'uses' => 'AccountController@settings', 'middleware' => 'auth']);
Route::get('/balance', ['as' => 'balance', 'uses' => 'AccountController@balance', 'middleware' => 'auth']);

// NewsController
Route::get('/news', ['as' => 'news', 'uses' => 'NewsController@index']);
Route::get('/news/{post_id}', ['as' => 'news.view', 'uses' => 'NewsController@view'])->where('post_id', '[0-9]+');

// NoticeController
Route::get('/notices', ['as' => 'notices', 'uses' => 'NoticeController@list', 'middleware' => 'auth']);
Route::get('/notices/{notice_id}', ['as' => 'notices.view', 'uses' => 'NoticeController@view', 'middleware' => 'auth'])->where('notice_id', '[0-9]+');

// AuthController
Route::get('/auth', ['as' => 'auth', 'uses' => 'AuthController@auth', 'middleware' => 'guest']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout', 'middleware' => 'auth']);
